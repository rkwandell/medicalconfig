<%@include file="template/pagetop.jsp" %>

<%
try {
// Set up the SQL statement
    String myQuery     = "select a.id, a.description, a.code as Type, " +
                         "a.amount, a.buttoncolor, a.buttontext, a.buttontextcolor, " +
                         "a.sequence, CASE WHEN showitem THEN 'Yes' ELSE 'No' END showitem from items a " +
                         "left outer join itemtypes b on b.id=a.typeid " +
                         "order by a.description, a.sequence";
    String url         = "items_d.jsp";
    String title       = "Items";

// Create an RWFiltered List object
    RWFilteredList lst = new RWFilteredList(io);
    RWHtmlTable htmTb  = new RWHtmlTable("620", "0");
    RWHtmlForm frm     = new RWHtmlForm();
    RWFieldSet fldSet  = new RWFieldSet();

// Set special attributes on the filtered list object
    String [] cw       = {"0", "250", "50", "75", "75", "75", "75", "50", "50" };
    String [] ch       = {"", "Description", "CPT<br>Code", "Charge<br>Amount", "Button<br>Color", "Button<br>Text", "Button<br>Text Color", "Seq", "Show" };

    lst.setTableWidth("700");
    lst.setTableBorder("0");
    lst.setCellPadding("3");
    lst.setAlternatingRowColors("#ffffff", "#cccccc");
    lst.setRoundedHeadings("#030089", "");
    lst.setTableHeading(title);
    lst.setUrlField(0);
    lst.setNumberOfColumnsForUrl(2);
    lst.setRowUrl(url);
    lst.setShowRowUrl(true);
    lst.setOnClickAction("window.open");
    lst.setOnClickOption("\"" + title + "\",\"width=500,height=400,scrollbars=no,left=100,top=100,\"");
    lst.setOnClickStyle("style=\"cursor: pointer; color: #2c57a7; font-weight: bold;\"");
    lst.setShowComboBoxes(true);
    lst.setUseCatalog(false);
    lst.setColumnWidth(cw);
//    lst.setUsePercentages(true);
    lst.setDivHeight(300);
    lst.setColumnAlignment(8, "CENTER");
    lst.setColumnFormat(3, "MONEY");

// Show the filtered list
    htmTb.replaceNewLineChar(false);

    int frameWidth=Integer.parseInt(lst.getTableWidth())+20;

    out.print(fldSet.getFieldSet(lst.getHtml(request, myQuery, ch), "style='width: " + frameWidth +"'", "Items", "style='font-size: 12; font-weight: bold;' align=center"));

    out.print(frm.startForm());
    out.print(frm.button("New " + title, "class=button onClick=window.open(\"" + url + "?id=0\",\"Locations\",\"width=500,height=400,scrollbars=no,left=100,top=100,\");" ));
    out.print(frm.endForm());

    session.setAttribute("parentLocation", self);

} catch (Exception e) {
    out.print(e);
}
%>
<%@ include file="template/pagebottom.jsp" %>