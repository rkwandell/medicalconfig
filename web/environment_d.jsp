<%@include file="template/pagetop.jsp" %>
<%@include file="ajax/ajaxstuff.jsp" %>
<title>Environment</title>
<script language="JavaScript" src="js/CheckDate.js"></script>
<script language="JavaScript" src="js/CheckLength.js"></script>

<script language="javascript">
  function submitForm(action) {
    var frmA=document.forms["frmInput"]
    frmA.method="POST"
    frmA.action=action
    frmA.submit()
  }  
</script>

<%
// Initialize local variables
    boolean showDefaultPrinter=true;
    boolean showPathInfo=true;

    try {
    ResultSet gRs=io.opnRS("select * from rwcatalog.globalsettings where id=1");
    if(gRs.next()) { 
        showDefaultPrinter=gRs.getBoolean("showdefaultprinter");
        showPathInfo=gRs.getBoolean("showpathinfo");
    }
    gRs.close();
    } catch (Exception gException) {
    }

    String myQuery          = "select id, facilityname, facilityaddress, suppliername, supplier, supplieraddress, " +
                              "supplierinfo, supplierphone, grp, pin, taxid, ssn, defaultresource, ";
    if(showDefaultPrinter) { myQuery += "defaultprinter, "; }
    if(showPathInfo) { myQuery += "templatepath, documentpath, browserpath, "; }
    myQuery += "officeemailaddress, apptemailsubject, apptemailmessage, showmessages, launchmonitor, billingmap, billprinttype, billmaprptoffset, recallvisitdays from environment ";

// Create a result set of the data for the form
    ResultSet lRs = io.opnRS(myQuery);

// Instantiate an RWInputForm and RWHtmlTable object
    RWInputForm frm = new RWInputForm(lRs);
    RWFilteredList lst = new RWFilteredList(io);
    RWHtmlTable htmTb = new RWHtmlTable ("800", "0");
    RWFieldSet fldSet = new RWFieldSet();
    
    String hoursQuery = "SELECT officehours.id, date, case when IfNull(resourceid,0)=0 then 'Office' else name end as name, " +
                        "morningstart, morningend, afternoonstart, afternoonend " +
                        "FROM officehours " + 
                        "left join resources on resources.id=resourceid " +
                        "where date>=current_date order by date";
    
    String ch [] = { "", "Date", "Provider", "AM<br>Start", "AM<br>End", "PM<br>Start", "PM<br>End" };
    String cw [] = { "0", "75", "150", "50", "50", "50", "50" };

    lst.setTableWidth("425");
    lst.setDivHeight(440);
    lst.setTableBorder("0");
    lst.setAlternatingRowColors("#ffffff", "#e0e0e0");
    lst.setColumnFilterState(1, false);
    lst.setColumnWidth(cw);
    lst.setOnClickAction(1, "showItem(event,'holidayhours.jsp?recordId=##idColumn##',0,0,txtHint) style='cursor: pointer;'");
    
    htmTb.replaceNewLineChar(false);

// Set display attributes for the input form
    frm.setTableWidth("400");
    frm.setTableBorder("0");
    frm.setDftTextBoxSize("35");
    frm.setDftTextAreaCols("35");
    frm.setDisplayDeleteButton(false);
    frm.setLabelBold(true);
    frm.setUpdateButtonText("  save  ");
    frm.setDeleteButtonText("remove");

// Get an input item with the record id to set the rcd and id fields
    out.print(htmTb.startTable());
    out.print(htmTb.startRow());
    out.print(htmTb.addCell(fldSet.getFieldSet(frm.getInputForm(), "style='width: 400; height: 520;'", "Environment Settings", "style='font-size: 12; font-weight: bold;'")));
    out.print(htmTb.addCell("", "width=10"));
    out.print(htmTb.addCell(fldSet.getFieldSet(lst.getHtml(request, hoursQuery, ch) + "<br><input type=button name=btn1 value=\"New\" class=button onClick=showItem(event,\"holidayhours.jsp\",0,0,txtHint)>", "style='width: 425; height: 520;'", "Holiday/Vacation Schedule", "style='font-size: 12; font-weight: bold;'")));
    out.print(htmTb.endRow());
    out.print(htmTb.endTable());

    session.setAttribute("returnUrl", "environment_d.jsp");
%>
<%@ include file="template/pagebottom.jsp" %>