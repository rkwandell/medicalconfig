<%@ include file="globalvariables.jsp" %>
<script>
  var timer = 15;
  function countdown() {
    if(timer > 0) {
      document.frmInput.countdown.value = timer;
      timer -= 1;
      setTimeout("countdown()",1000);
    } else {
      location.href="instantmessages.jsp";
    }
  }
</script>

<body onLoad="countdown()" topmargin="0" leftmargin="0" bottommargin="0" style="background: 000066">
    <form name=frmInput>
    <input type=hidden name=countdown>
    </form>

<%
    Messages messages = (Messages)session.getAttribute("messages");
    String patientId  = request.getParameter("patientId");

// Create a new messages object if one does not exist
    if(messages == null) { messages = new Messages(altIo); }

// Save the message object as a session variable
    session.setAttribute("messages", messages);

// If patient id is null, set the patient id 0 to get all messages
    if(patientId == null) { patientId = "0"; }

// Show the messages
    out.print("<table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"100%\"><tr><td align=center><table border=\"0\" cellpadding=\"0\" cellspacing=\"0\"><tr><td align=left>");
    out.print(messages.getMessages(Integer.parseInt(patientId)));
    out.print("</td></tr></table></td></tr></table>");

// Close the Connection
    altIo.getConnection().close();
    altIo.setConnection(null);
%>
</body>
