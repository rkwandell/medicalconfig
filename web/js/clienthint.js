var xmlHttp
var objectName

function showHint(str) {
    objectName="txtHint";
    if (str.length==0) { 
      document.getElementById("txtHint").innerHTML="";
      return;
    }
    xmlHttp=GetXmlHttpObject();
    if (xmlHttp==null) {
      alert ("Your browser does not support AJAX!");
      return;
    }
    var url="gethint.jsp";
    url=url+"?q="+str;
    url=url+"&sid="+Math.random();
    xmlHttp.onreadystatechange=stateChanged;
    xmlHttp.open("GET",url,true);
    xmlHttp.send(null);
} 

function showItemDetails(jspName,recId,patientId,parentUrl,objName) {
    objectName=objName

    if (recId.length==0) { 
      document.getElementById(objName).innerHTML="";
      return;
    }
    
    xmlHttp=GetXmlHttpObject();
    if (xmlHttp==null) {
      alert ("Your browser does not support AJAX!");
      return;
    } 
    var url=jspName;

    if(jspName.indexOf("?")>-1) {
        url=url+"&";
    } else {
        url=url+"?";
    }

    url=url+"id="+recId+"&patientid="+patientId+"&parentUrl="+parentUrl;
    url=url+"&sid="+Math.random();
    xmlHttp.onreadystatechange=stateChanged;
    xmlHttp.open("GET",url,true);
    xmlHttp.send(null);
} 

function showForm(jspName,recId,patientId,parentUrl) {
    objectName="txtHint"
    if (recId.length==0) { 
      document.getElementById("txtHint").innerHTML="";
      return;
    }
    xmlHttp=GetXmlHttpObject();
    if (xmlHttp==null) {
      alert ("Your browser does not support AJAX!");
      return;
    } 
    var url=jspName;
    if(jspName.indexOf("?")>-1) {
        url=url+"&";
    } else {
        url=url+"?";
    }

    url=url+"id="+recId+"&patientid="+patientId+"&parentUrl="+parentUrl;
    url=url+"&sid="+Math.random();
    xmlHttp.onreadystatechange=stateChanged;
    xmlHttp.open("GET",url,true);
    xmlHttp.send(null);
}  

function stateChanged() { 
    if (xmlHttp.readyState==4) { 
        document.getElementById(objectName).innerHTML=xmlHttp.responseText;
        ajaxComplete=true;
    }
}

function GetXmlHttpObject() {
    var xmlHttp=null;
    try  {
      // Firefox, Opera 8.0+, Safari
      xmlHttp=new XMLHttpRequest();
    } catch (e) {
      // Internet Explorer
      try {
        xmlHttp=new ActiveXObject("Msxml2.XMLHTTP");
      } catch (e) {
        xmlHttp=new ActiveXObject("Microsoft.XMLHTTP");
      }
    }
    return xmlHttp;
}
