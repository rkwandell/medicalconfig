<%-- 
    Document   : itemgroup_d
    Created on : Aug 10, 2009, 11:24:58 AM
    Author     : Randy
--%>
<%@ include file="globalvariables.jsp" %>
<title>Item Group</title>
<SCRIPT language=JavaScript>
<!-- 
function win(parent){
if(parent != null) { window.opener.location.href=parent }
self.close();
//-->
}
</SCRIPT>

<%
// Initialize local variables
    String ID = request.getParameter("id");
    String parentLocation=(String)session.getAttribute("parentLocation");

// If the id in the request is null or an empty string make it 0 to indicate an add
    if(ID == null || ID.equals("")) {
        ID = "0";
    }

    if(request.getMethod().equals("POST")) {
        String groupId=request.getParameter("groupId");

        PreparedStatement iPs=io.getConnection().prepareStatement("insert into itemgroup (itemid, groupid, sequence) values(?,?,?)");
        PreparedStatement dPs=io.getConnection().prepareStatement("delete from itemgroup where groupid=" + groupId);
        dPs.execute();

        for(Enumeration e=request.getParameterNames();e.hasMoreElements();) {
            String param=(String)e.nextElement();
            if(param.length()>3 && param.substring(0,3).equals("chk")) {
                String sequence=request.getParameter("sequence" + param.substring(3));
                iPs.setString(1,param.substring(3));
                iPs.setString(2,groupId);
                iPs.setString(3,sequence);
                iPs.execute();
            }
        }

        out.println("<script>win('" + parentLocation + "')</script>");
    } else {
        String myQuery="select id, concat('<input type=\"checkbox\" name=\"chk',id,'\" '," +
                       "case when ifnull((select itemid from itemgroup where groupid=" + ID + " and itemid= i.id),'')='' then '' else 'CHECKED' END,'>') as chkbox," +
                       "concat(case when ifnull(code,'')='' then '' else concat(code,' - ') end, description) as description, " +
                       "concat('<input type=\"text\" name=\"sequence',ID,'\" value=\"0\" style=\"text-align: right; width: 50px;\">') as sequence " +
                       "from items i where not itemgroup";


    //    ResultSet lRs=io.opnRS("select id, itemid, groupid from itemgroup where groupid=" + ID);
        ResultSet itemRs=io.opnRS("select description from items where id=" + ID);
        if(itemRs.next()) {
            RWFilteredList lst = new RWFilteredList(io);

            String [] cw       = {"0", "50", "350"};
            String [] ch       = {"", "Sel", "Description"};

            lst.setTableWidth("400");
            lst.setTableBorder("0");
            lst.setCellPadding("3");
            lst.setAlternatingRowColors("#ffffff", "#cccccc");
            lst.setRoundedHeadings("#030089", "");
            lst.setTableHeading("Items In Group");
            lst.setUrlField(0);
            lst.setNumberOfColumnsForUrl(0);
            lst.setRowUrl("");
            lst.setShowRowUrl(false);
            lst.setShowComboBoxes(false);
            lst.setUseCatalog(false);
            lst.setColumnWidth(cw);
            lst.setDivHeight(250);

            out.print("<div align='center' width='100%'><b>" + itemRs.getString("description") + "</b></div>\n");
            out.print("<form name=frmInput method='post'>\n");
            out.print("<input type='hidden' name='groupId' id='groupId' value='" + ID + "'>\n");
            out.print(lst.getHtml(null, myQuery, ch));
            out.print("<input type='submit' value='save' class='button'></form>\n");
        } else {
            out.print("<div align='center' width='100%'>Inventory item not found</div>");        
        }
    }
    
    session.setAttribute("returnUrl", "");
%>
