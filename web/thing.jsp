
<link rel="stylesheet" type="text/css" href="css/stylesheet.css" title="stylesheet">
<script language="JavaScript" src="js/date-picker.js"></script>
<script language="JavaScript" src="js/CheckDate.js"></script>
<script language="JavaScript" src="js/CheckLength.js"></script>
<script language="JavaScript" src="js/colorpicker.js"></script>
<script language="JavaScript" src="js/datechecker.js"></script>
<script language="JavaScript" src="js/dFilter.js"></script>
<script language="JavaScript" src="js/currency.js"></script>
<script language="JavaScript" src="js/checkemailaddress.js"></script>



<title>Providers</title>

<script language="javascript">
  function submitForm(action) {
    var frmA=document.forms["frmInput"]
    frmA.method="POST"
    frmA.action=action
    frmA.submit()
  }

function loadMask() {
    for(y=0; y<document.forms.length; y++) {
        var frmA = document.forms[y];
        for(x=0; x<frmA.elements.length; x++) {
            if(frmA.elements[x].id.substring(0,9) == "maskValue") {
                mask = frmA.elements[x].id.substring(9);
                if(frmA.elements[x].value != '' && frmA.elements[x].value != '0') {
                    dFilter(21, frmA.elements[x], mask);
                } else {
                    frmA.elements[x].value = '';
                }
            }
        }
    }
}

function fillHidden(what) {
alert(what.name);
    hiddenField = what.name.substring(4);
    obj=document.forms[what.form.name].elements[hiddenField];
    obj.value=dFilterStrip(what.value, what.id.substring(9));
}


var dFilterStep

function dFilterStrip (dFilterTemp, dFilterMask)
{
    dFilterMask = replace(dFilterMask,'#','');
    for (dFilterStep = 0; dFilterStep < dFilterMask.length++; dFilterStep++)
		{
		    dFilterTemp = replace(dFilterTemp,dFilterMask.substring(dFilterStep,dFilterStep+1),'');
		}
		return dFilterTemp;
}

function dFilterMax (dFilterMask)
{
 		dFilterTemp = dFilterMask;
    for (dFilterStep = 0; dFilterStep < (dFilterMask.length+1); dFilterStep++)
		{
		 		if (dFilterMask.charAt(dFilterStep)!='#')
				{
		        dFilterTemp = replace(dFilterTemp,dFilterMask.charAt(dFilterStep),'');
				}
		}
		return dFilterTemp.length;
}

function dFilter (key, textbox, dFilterMask)
{
		dFilterNum = dFilterStrip(textbox.value, dFilterMask);
		
		if (key==9)
		{
		    return true;
		}
		else if (key==8&&dFilterNum.length!=0)
		{
		 	 	dFilterNum = '';
		}
 	  else if ( ((key>47&&key<58)||(key>95&&key<106)) && dFilterNum.length<dFilterMax(dFilterMask) )
		{ if(key>=96&&key<=105) {key = key -48;}
        dFilterNum=dFilterNum+String.fromCharCode(key);
		}

		var dFilterFinal='';
    for (dFilterStep = 0; dFilterStep < dFilterMask.length; dFilterStep++)
		{
        if (dFilterMask.charAt(dFilterStep)=='#')
				{
					  if (dFilterNum.length!=0)
					  {
				        dFilterFinal = dFilterFinal + dFilterNum.charAt(0);
					      dFilterNum = dFilterNum.substring(1,dFilterNum.length);
					  }
				    else
				    {
				        dFilterFinal = dFilterFinal + "";
				    }
				}
		 		else if (dFilterMask.charAt(dFilterStep)!='#')
				{
				    dFilterFinal = dFilterFinal + dFilterMask.charAt(dFilterStep); 			
				}
//		    dFilterTemp = replace(dFilterTemp,dFilterMask.substring(dFilterStep,dFilterStep+1),'');
		}


		textbox.value = dFilterFinal;
    return false;
}

function replace(fullString,text,by) {
// Replaces text with by in string
    var strLength = fullString.length, txtLength = text.length;
    if ((strLength == 0) || (txtLength == 0)) return fullString;

    var i = fullString.indexOf(text);
    if ((!i) && (text != fullString.substring(0,txtLength))) return fullString;
    if (i == -1) return fullString;

    var newstr = fullString.substring(0,i) + by;

    if (i+txtLength < strLength)
        newstr += replace(fullString.substring(i+txtLength,strLength),text,by);

    return newstr;
}

</script>

 <tr >
  <td align=CENTER valign=TOP height=300 colspan=1 style="border-bottom: black solid 1px; border-left: black solid 1px; border-right: black solid 1px;"><form name=frmInput>
<table width="400" border="0" cellSpacing="0" cellPadding="0">
 <tr >
  <td align=LEFT valign=TOP ><b>Provider Name</b></td>
  <td align=LEFT valign=TOP ><input type=TEXT name=name value="Allied Benefit Systems, INC" maxlength=100 size=35 class=tBoxText></td>
 </tr>
 <tr >
  <td align=LEFT valign=TOP ><b>Address</b></td>
  <td align=LEFT valign=TOP ><textarea  name=address cols="35" rows="3" class=tAreaText>PO Box 909786-60690

Chicago, IL  60690</textarea></td>
 </tr>
 <tr >
  <td align=LEFT valign=TOP ><b>Phone Number</b></td>
  <td align=LEFT valign=TOP ><input type=HIDDEN name=phonenumber value="0"><input type=TEXT name=maskphonenumber value="0" maxlength=14 size=14 class=tBoxText  id="maskValue(###) ###-####" onKeyDown="javascript:return dFilter (event.keyCode, this, '(###) ###-####');" onKeyUp="fillHidden(this); if(this.value=='() -')this.value='';">
</td>
 </tr>
 <tr >
  <td align=LEFT valign=TOP ><b>Extension</b></td>
  <td align=LEFT valign=TOP ><input type=TEXT name=extension value="00000" maxlength=5 size=5 class=tBoxText></td>
 </tr>
 <tr >
  <td align=LEFT valign=TOP ><b>Contact Name</b></td>
  <td align=LEFT valign=TOP ><input type=TEXT name=contactname maxlength=100 size=35 class=tBoxText></td>
 </tr>
 <tr >
  <td align=LEFT valign=TOP ><b>E-mail Address</b></td>
  <td align=LEFT valign=TOP ><input type=TEXT name=contactemail maxlength=100 size=35 class=tBoxText></td>
 </tr>
 <tr >
  <td align=LEFT valign=TOP ><b>Billing Address</b></td>
  <td align=LEFT valign=TOP ><textarea  name=billingaddress cols="35" rows="3" class=tAreaText></textarea></td>
 </tr>
 <tr >
  <td align=LEFT valign=TOP ><b>Payer</b></td>
  <td align=LEFT valign=TOP ><input type=TEXT name=payer value="0" maxlength=11 size=11 onBlur="return checkban(this)" class=tBoxText></td>
 </tr>
 <tr >
  <td align=LEFT valign=TOP ><b>Setof</b></td>
  <td align=LEFT valign=TOP ><input type=TEXT name=setof value="0" maxlength=11 size=11 onBlur="return checkban(this)" class=tBoxText></td>
 </tr>
 <tr >
  <td align=LEFT valign=TOP ><b>Grouptaxid</b></td>
  <td align=LEFT valign=TOP ><input type=TEXT name=grouptaxid maxlength=30 size=30 class=tBoxText></td>
 </tr>
 <tr >
  <td align=LEFT valign=TOP ><b>Grouppractice</b></td>
  <td align=LEFT valign=TOP ><input type=TEXT name=grouppractice maxlength=45 size=35 class=tBoxText></td>
 </tr>
 <tr >
  <td align=LEFT valign=TOP ><b>Payerid</b></td>
  <td align=LEFT valign=TOP ><input type=TEXT name=payerid maxlength=45 size=35 class=tBoxText></td>
 </tr>
 <tr >
  <td align=LEFT valign=TOP ><b>Tds</b></td>
  <td align=LEFT valign=TOP ><input type=TEXT name=tds value="0" maxlength=5 size=5 onBlur="return checkban(this)" class=tBoxText></td>
 </tr>
 <tr >
  <td align=LEFT valign=TOP ><b>Pos</b></td>
  <td align=LEFT valign=TOP ><input type=TEXT name=pos value="0" maxlength=5 size=5 onBlur="return checkban(this)" class=tBoxText></td>
 </tr>
 <tr >
  <td align=LEFT valign=TOP ><b>Ediid</b></td>
  <td align=LEFT valign=TOP ><input type=TEXT name=ediid maxlength=45 size=35 class=tBoxText></td>
 </tr>
 <tr >
  <td align=LEFT valign=TOP ><b>Claimoffice</b></td>
  <td align=LEFT valign=TOP ><input type=TEXT name=claimoffice maxlength=45 size=35 class=tBoxText></td>
 </tr>
 <tr >
  <td align=LEFT valign=TOP ><b>Necid</b></td>
  <td align=LEFT valign=TOP ><input type=TEXT name=necid maxlength=45 size=35 class=tBoxText></td>
 </tr>
 <tr >
  <td align=LEFT valign=TOP ><b>Practice</b></td>
  <td align=LEFT valign=TOP ><input type=TEXT name=practice value="0" maxlength=11 size=11 onBlur="return checkban(this)" class=tBoxText></td>
 </tr>
 <tr >
  <td align=LEFT valign=TOP ><b>Assignment</b></td>
  <td align=LEFT valign=TOP ><input type=CHECKBOX name=assignment_cb onClick=document.getElementById('assignment').value=document.getElementById('assignment_cb').checked ; CHECKED><input type=hidden name=assignment value=true></td>
 </tr>
 <tr >
  <td align=LEFT valign=TOP ><b>Category</b></td>
  <td align=LEFT valign=TOP ><SELECT NAME=category size=1  onBlur="return checkban(this)" class=cBoxText >
</SELECT>
</td>
 </tr>
 <tr >
  <td align=LEFT valign=TOP ><b>Reserved</b></td>
  <td align=LEFT valign=TOP ><input type=CHECKBOX name=reserved_cb onClick=document.getElementById('reserved').value=document.getElementById('reserved_cb').checked ;><input type=hidden name=reserved value=false></td>
 </tr>
 <tr >
  <td align=LEFT valign=TOP ><b>showinbox11</b></td>
  <td align=LEFT valign=TOP ><input type=CHECKBOX name=showinbox11_cb onClick=document.getElementById('showinbox11').value=document.getElementById('showinbox11_cb').checked ; CHECKED><input type=hidden name=showinbox11 value=true></td>
 </tr>
 <tr >
  <td align=LEFT valign=TOP ><b>Show Qualifier</b></td>
  <td align=LEFT valign=TOP ><input type=CHECKBOX name=showqualifier_cb onClick=document.getElementById('showqualifier').value=document.getElementById('showqualifier_cb').checked ;><input type=hidden name=showqualifier value=false></td>
 </tr>
 <tr >
  <td align=LEFT valign=TOP ><b>Box 19 Value</b></td>
  <td align=LEFT valign=TOP ><input type=TEXT name=box19 maxlength=45 size=35 class=tBoxText></td>
 </tr>
 <tr >
  <td align=LEFT valign=TOP colspan=2 height=5px></td>
 </tr>
</table>
<input type=BUTTON name=btnSubmit value="  save  " class=button onClick=submitForm('updaterecord.jsp?fileName=providers')>&nbsp;&nbsp;<input type=BUTTON name=btnDelete value="remove" class=button onClick=submitForm('updaterecord.jsp?fileName=providers&delete=Y')><input type=HIDDEN name=rcd value="107">
<input type=HIDDEN name=ID value="107">
</form>
</td>
 </tr>
</table>

