<%
    String userName=request.getParameter("userName");

    if(userName == null || userName.trim().equals("")) {
        out.print("You are not authorized to this application");
    } else {
        response.sendRedirect("setdbname.jsp?databaseName="+userName);
    }
%>