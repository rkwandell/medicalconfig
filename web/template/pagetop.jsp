<%@ include file="/globalvariables.jsp" %>
<%
    response.setHeader("Cache-Control", "no-cache"); 
    response.setHeader("Pragma", "no-cache"); 
    response.setHeader("Pragma", "no-store"); 

    String absoluteRoot = System.getProperty("catalina.home") + "\\webapps\\comsys";

    String self = request.getRequestURI();
    String wordSelf = "";
    String excelSelf = "";
    String printSelf = "";
    String content = request.getParameter("content");
    String parm = "";
    StringBuffer parms = new StringBuffer("");

    String libraryName="";
    if(databaseName != null) { libraryName=databaseName; }

    if (content == null) {
        content = "HTML";
    }
    for( Enumeration en = request.getParameterNames(); en.hasMoreElements(); ) {
        if (parms.toString()!=""){
            parms.append("&");
        }
        parm = ( String) en.nextElement();
        parms.append(parm + "=" + request.getParameter(parm)); 
    }
    if (parms.toString().length()>0){
        excelSelf = self + "?" + parms.toString() + "&content=EXCEL";
        printSelf = self + "?" + parms.toString() + "&content=PRINT";
        wordSelf = self + "?" + parms.toString() + "&content=WORD";
    } else {
        excelSelf = self + "?content=EXCEL";
        printSelf = self + "?content=PRINT";
        wordSelf = self + "?content=WORD";
    }
    
    if (content.equals("EXCEL")) {
        response.setContentType("application/vnd.ms-excel");
    }
    if (content.equals("WORD")) {
        response.setContentType("application/msword");
    }

    if (!content.equals("WORD") && !content.equals("EXCEL")) {
%>
<html>
<head>
<link rel="stylesheet" type="text/css" href="css/stylesheet.css" title="csstylesheet">
<title>Chiropractic Office Management System</title>

</head>
<body topmargin="0" leftmargin="0" style="background: #000066;  background-image: url('/medicaldocs/bg_page.gif');">
<%
    }
    
if (content.equals("HTML")) {
%>
<table height="100%" width="100%" border=0 cellpadding=0 cellspacing=0>
  <tr>
    <td height=75 colspan=2>
        <table  width=100% border=0 cellpadding=0 cellspacing=0>
            <tr>
                <td width="45%" bgcolor=white align=left><img src="/medicaldocs/<% out.print(libraryName); %>/images/topleft.JPG" height=75 alt=""></td>
                <td width="10%" bgcolor=white align=left><img src="/medicaldocs/<% out.print(libraryName); %>/images/topcenter.JPG" height=75 alt=""></td>
                <td width="45%" bgcolor=white align=right><img src="/medicaldocs/<% out.print(libraryName); %>/images/topright.JPG" height=75 alt=""></td>
            </tr>
            <tr>
                <td width="100%" colspan=3 height=3 bgcolor=navy></td>
            </tr>
        </table>
    </td>   
  </tr>
  <tr>
    <td align=center valign=top>
      <TABLE cellSpacing=5 border=0 width="1024">
	<TR>
          <TD valign=top>
<!-- start of body -->              
<%}%>   
<%@ include file="/tabtop.jsp" %>
