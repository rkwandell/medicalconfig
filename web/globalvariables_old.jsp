<%@ page import="medical.*, tools.*, tools.utils.*, java.sql.*, java.util.*, java.math.* " %>
<link rel="stylesheet" type="text/css" href="css/stylesheet.css" title="stylesheet">
<script language="JavaScript" src="js/date-picker.js"></script>
<script language="JavaScript" src="js/CheckDate.js"></script>
<script language="JavaScript" src="js/CheckLength.js"></script>
<script language="JavaScript" src="js/colorpicker.js"></script>
<script language="JavaScript" src="js/datechecker.js"></script>
<script language="JavaScript" src="js/dFilter.js"></script>
<script language="JavaScript" src="js/currency.js"></script>
<script language="JavaScript" src="js/checkemailaddress.js"></script>

<%
    RWConnMgr io        = (RWConnMgr)session.getAttribute("connMgr");
    RWConnMgr altIo     = (RWConnMgr)session.getAttribute("altConnMgr");
    Patient patient     = (Patient)session.getAttribute("patient");
    Environment env     = (Environment)session.getAttribute("env");
    Visit visit         = (Visit)session.getAttribute("visit");
    Location location   = (Location)session.getAttribute("location");

    if(io == null) {
        io = new RWConnMgr("localhost", "medical", "rwtools", "rwtools", io.MYSQL);
        session.setAttribute("connMgr", io);
    }

    if(io.getConnection() == null) {
        io.setConnection(io.opnmySqlConn());
    } else {
        io.getConnection().close();
        io.setConnection(null);
        io.setConnection(io.opnmySqlConn());
    }

    if(altIo == null) {
        altIo = new RWConnMgr("localhost", "medical", "rwtools", "rwtools", altIo.MYSQL);
        session.setAttribute("altConnMgr", altIo);
    }

    if(altIo.getConnection() == null) {
        altIo.setConnection(altIo.opnmySqlConn());
    }

    if(env == null) {
        env = new Environment(io);
        session.setAttribute("env", env);
    } else {
        env.setConnMgr(io);
        env.refresh();
    }

    if(patient == null) {
        patient = new Patient(io, "0");
        session.setAttribute("patient", patient);
    } else {
        patient.setConnMgr(io);
        patient.refresh();
    }

    if(visit == null) {
        visit = new Visit(io, "0");
        session.setAttribute("visit", visit);
    } else {
        visit.setConnMgr(io);
        visit.setId(visit.getId());
    }

    if(location == null) {
        location = new Location(io, 0);
        session.setAttribute("location", location);
    } else {
        location.setConnMgr(io);
        location.setId(location.getId());
    }

%>