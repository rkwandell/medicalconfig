<%@include file="template/pagetop.jsp" %>

<%
try {
// Set up the SQL statement
    String myQuery     = "select id, " + 
                         "CASE WHEN reserved THEN name ELSE concat(name,' - '," +
                         "case when substr(providers.address,length(providers.address)-4,1)='-' then " +
                         "substr(address,(locate(_latin1'\r',address) + 2),length(address)-10-(locate(_latin1'\r',address) + 2)) " +
                         "else " +
                         "substr(address,(locate(_latin1'\r',address) + 2),length(address)-5-(locate(_latin1'\r',address) + 2)) " +
                         "end) END as name, " +
                         "phonenumber, " +
                         "CASE WHEN extension=0 THEN '' ELSE extension END AS extension, " +
                         "CASE WHEN effectivedate='0001-01-01' THEN '' ELSE date_format(effectivedate,'%m/%d/%y') END as effectivedate, " +
                         "CASE WHEN numberofvisits=0 THEN '' ELSE numberofvisits END AS numberofvisits, " +
                         "contactname, contactemail, " +
                         "case when billprinttype=0 then '' " +
                         "when billprinttype=1 then 'File' " +
                         "when billprinttype=2 then 'PDF' end as printtype, billingmap, billmaprptoffset " +
                         "from providers order by reserved desc, name";
    String url         = "providers_d.jsp";
    String title       = "Payers";

// Create an RWFiltered List object
    RWFilteredList lst = new RWFilteredList(io);
    RWHtmlTable htmTb  = new RWHtmlTable("620", "0");
    RWHtmlForm frm     = new RWHtmlForm();
    RWFieldSet fldSet  = new RWFieldSet();

    String [] colWidth = { "0", "250", "100", "50", "100", "75", "100", "100", "75", "75", "75" };
    String [] colHeading = { "", "Name", "Phone Number", "Ext", "Renewal Date", "Visits/Year", "Contact", "E-mail", "Print Type" };

// Set special attributes on the filtered list object
    lst.setTableWidth("825");
    lst.setTableBorder("0");
    lst.setCellPadding("3");
    lst.setAlternatingRowColors("#ffffff", "#cccccc");
    lst.setRoundedHeadings("#030089", "");
//    lst.setTableHeading(title);
    lst.setUrlField(0);
    lst.setNumberOfColumnsForUrl(2);
    lst.setRowUrl(url);
    lst.setShowRowUrl(true);
    lst.setOnClickAction("window.open");
    lst.setOnClickOption("\"" + title + "\",\"width=450,height=370,scrollbars=no,left=100,top=100,\"");
    lst.setOnClickStyle("style=\"cursor: pointer; color: #2c57a7; font-weight: bold;\"");
    lst.setShowComboBoxes(false);
    lst.setUseCatalog(true);
    lst.setDivHeight(300);
    lst.setColumnWidth(colWidth);
    lst.setColumnAlignment(2, "CENTER");
    lst.setColumnAlignment(3, "CENTER");

    lst.setColumnFormat(2,"(###) ###-####");

// Show the filtered list
    htmTb.replaceNewLineChar(false);

    out.print(fldSet.getFieldSet(lst.getHtml(myQuery, colHeading), "style='width: " + lst.getTableWidth() +"'", "Payers", "style='font-size: 12; font-weight: bold;' align=center"));

    out.print(frm.startForm());
    out.print(frm.button("New " + title, "class=button onClick=window.open(\"" + url + "?id=0\",\"Payers\",\"width=450,height=370,scrollbars=no,left=100,top=100,\");" ));
    out.print(frm.endForm());

    session.setAttribute("parentLocation", self);

} catch (Exception e) {
    out.print(e);
}
%>
<%@ include file="template/pagebottom.jsp" %>