<%@ include file="template/pagetop.jsp" %>
<script>
    function startBackup() {
        frm1.submit();
    }
</script>
<%
    RWHtmlForm frm=new RWHtmlForm();
    RWHtmlTable htmTb=new RWHtmlTable("100%", "0");
    String backUp=request.getParameter("backUp");
    String codeSource="";
    String dataSource="";
    String target="";

    if(backUp == null) {
        ResultSet backupRs=io.opnRS("select * from backupparameters");
        if(backupRs.next()) {
            out.print(frm.startForm());
            out.print(frm.hidden("Y", "backUp"));
            out.print(htmTb.startTable());
            out.print(htmTb.startRow());
            out.print(htmTb.addCell("<b>Code Source</b>"));
            out.print(htmTb.addCell(frm.textBox(backupRs.getString("codesource"), "codesource", "class=tBoxText size=80 maxwidth=255")));
            out.print(htmTb.endRow());
            out.print(htmTb.startRow());
            out.print(htmTb.addCell("<b>Data Source</b>"));
            out.print(htmTb.addCell(frm.textBox(backupRs.getString("datasource"), "datasource", "class=tBoxText size=80 maxwidth=255")));
            out.print(htmTb.endRow());
            out.print(htmTb.startRow());
            out.print(htmTb.addCell("<b>Target</b>"));
            out.print(htmTb.addCell(frm.textBox(backupRs.getString("target"), "target", "class=tBoxText size=80 maxwidth=255")));
            out.print(htmTb.endRow());
            out.print(frm.endForm());
        }

        out.print(htmTb.startTable());
        out.print(htmTb.startRow());
        out.print(htmTb.addCell(frm.button("start backup", "onClick=startBackup() class=button"), htmTb.CENTER));
        out.print(htmTb.endRow());
        out.print(htmTb.endTable());
    } else {
        codeSource=request.getParameter("codesource");
        dataSource=request.getParameter("datasource");
        target=request.getParameter("target");
        boolean insertRecord=false;

        ResultSet tmpRs=io.opnUpdatableRS("select * from backupparameters");
        if(!tmpRs.next()) {
            tmpRs.moveToInsertRow();
            insertRecord=true;
        }
        tmpRs.updateString("codesource", codeSource);
        tmpRs.updateString("datasource", dataSource);
        tmpRs.updateString("target", target);

        if(insertRecord) {
            tmpRs.insertRow();
        } else {
            tmpRs.updateRow();
        }

        if(!codeSource.endsWith("\\")) { codeSource += "\\"; }
        if(!dataSource.endsWith("\\")) { dataSource += "\\"; }
        if(!target.endsWith("\\")) { target += "\\"; }

        DoBackup doBackup=new DoBackup();

        // Bakcup the MySQL databases
        doBackup.setTargetPath(target + tools.utils.Format.formatDate(new java.util.Date(), "yyyy-MM-dd") + "\\data\\medical");
        doBackup.setSourcePath(dataSource + "medical");
        doBackup.backup();

        doBackup.setTargetPath(target + tools.utils.Format.formatDate(new java.util.Date(), "yyyy-MM-dd") + "\\data\\rwcatalog");
        doBackup.setSourcePath(dataSource + "rwcatalog");
        doBackup.backup();

        // Backup ChiroPractice
        doBackup.setTargetPath(target + tools.utils.Format.formatDate(new java.util.Date(), "yyyy-MM-dd") + "\\code\\medical");
        doBackup.setSourcePath(codeSource + "medical");
        doBackup.backup();

        doBackup.setTargetPath(target + tools.utils.Format.formatDate(new java.util.Date(), "yyyy-MM-dd") + "\\code\\medicalconfig");
        doBackup.setSourcePath(codeSource + "medicalconfig");
        doBackup.backup();

        doBackup=null;

        out.print(htmTb.startTable());
        out.print(htmTb.startRow());
        out.print(htmTb.addCell("Backup complete", htmTb.CENTER));
        out.print(htmTb.endRow());
        out.print(htmTb.endTable());
    }
%>
<%@ include file="template/pagebottom.jsp" %>