/*
 * AppointmentPage.java
 *
 * Created on November 29, 2005, 8:36 PM
 *
 * To change this template, choose Tools | Options and locate the template under
 * the Source Creation and Management node. Right-click the template and choose
 * Open. You can then make changes to the template in the Source Editor.
 */

package medical;

import tools.*;
import tools.utils.*;
import java.sql.*;
import java.util.*;
import javax.servlet.http.HttpServletRequest;

/**
 *
 * @author BR Online Solutions
 */
public class AppointmentPage {

    private RWConnMgr io;
    private RWHtmlTable htmTb;
    private RWHtmlTable outerHtmTb;
    private RWCalendar cal;
    private RWHtmlForm frm;
    
    private String self = "";
    private String btn1 = "";
    private String btn2 = "";
    private String date = "";
    private String time = "";
    private String srchString = "";
    private String apptDate = "";
    private String apptTime = "";
    private String platterColor = "#ffffff";

    private int type = 0;
    private int intervals = 0;
    private int scrollUp = 0;
    private int scrollDown = 0;
    private int scrollValue = 0;
    private int addDays = 0;
    private int apptId = 0;
    private int patientId = 0;
    
    private Appointment thisAppointment;
    private AppointmentCalendar thisApptCal;
    private Patient thisPatient;
    private HttpServletRequest request;
    
    /** Creates a new instance of AppointmentPage */
    public AppointmentPage(RWConnMgr newIo, String url) {
        try {
            io=newIo;

            htmTb = new RWHtmlTable("","0");
            frm= new RWHtmlForm();
            htmTb.replaceNLChar = false;
            outerHtmTb = new RWHtmlTable("","0");
            outerHtmTb.setCellPadding("1");
            outerHtmTb.replaceNLChar = false;

            self=url;

            cal = new RWCalendar(Integer.parseInt(Format.formatDate(new java.util.Date(), "yyyy")), 
                                            Format.formatDate(new java.util.Date(), "MMMM"));
            cal.setCalendar();
            cal.showMonthCombo(true);
            cal.showYearCombo(true);
            cal.setLongMonth(true);
            cal.setDayUrl(self);
            cal.setBgColorForToday("yellow");
            cal.setBgColorForSelected("orange");
            cal.showSelectedDate(true);
            int hourOfDay = Calendar.getInstance().get(Calendar.HOUR_OF_DAY);
            thisApptCal = new AppointmentCalendar();

            thisApptCal.setApptTypesRs(io.opnRS("select * from appointmenttypes"));

            thisApptCal.setColsToGenerate(9);
            thisApptCal.setRowsToGenerate(23);
            thisApptCal.setStartMinute(00);
            thisApptCal.setIncrementMinutes(15);
            thisApptCal.setCellWidth("58");
            thisApptCal.setTimeURL(url);
            thisApptCal.setScrollURL(url);
            thisApptCal.setScrollTimeIncrements(28);
            if (hourOfDay>11) {
                thisApptCal.setStartHour(13);
            } else {
                thisApptCal.setStartHour(06);
            }

            thisAppointment = new Appointment(io, "0");
            thisPatient = new Patient(io, "0");
            
        } catch (Exception e) {
        }

    }

//---------------------------------------------------------------------------------------------//
    /**
     * Process a request's parameters.
     */
//---------------------------------------------------------------------------------------------//
    public void processRequestParameters(HttpServletRequest request) throws Exception {

        // If the "show arrivals" checkbox is sent, then set the boolena
        if (request.getParameter("showarrivals")!=null) {
            thisApptCal.setShowArrivals(Boolean.parseBoolean(request.getParameter("showarrivals")));
        }        
        
        // If a new patient was selected, reset the appointment ID to zero
        if (request.getParameter("srchPatientId")!=null) {
            apptId=0;
            thisApptCal.setSelectedAppointment(0);
            patientId=Integer.parseInt(request.getParameter("srchPatientId"));
        }
        
        // Process Delete Request
        if (request.getParameter("btn2")!=null) {
            deleteAppointment();
        }

        // Process Update Request
        if (request.getParameter("btn1")!=null) {
            apptTime = request.getParameter("time");
            apptDate = request.getParameter("date");
            intervals = Integer.parseInt(request.getParameter("intervals"));
            type = Integer.parseInt(request.getParameter("type"));
            apptDate=apptDate.substring(6,10) + "-" + apptDate.substring(0,2) + "-" + apptDate.substring(3,5);
            updateAppointment();
        }

        // Schedule an appointment
        if (request.getParameter("apptTime")!=null && patientId>0) {
            apptDate = request.getParameter("apptDate");
            apptTime = request.getParameter("apptTime") + ":00";
            type = 0;
            intervals=0;
            updateAppointment();
        }
        
        srchString = "*EMPTY";

        if (request.getParameter("year")!=null) {
            cal.setYear(Integer.parseInt(request.getParameter("year")));
        }
        if (request.getParameter("month")!=null) {
            cal.setMonth(request.getParameter("month"));
        }
        cal.setCalendar();

        if (request.getParameter("date")!=null && request.getParameter("btn2")==null
                && request.getParameter("btn1")==null) {
            date=request.getParameter("date");
            int yr = Integer.parseInt(date.substring(0,4))-1900;
            int mt = Integer.parseInt(date.substring(4,6))-1;
            int dy = Integer.parseInt(date.substring(6,8));
            thisApptCal.setDate(new java.util.Date(yr,mt,dy));
        }

        if (request.getParameter("adddays")!=null) {
            addDays=Integer.parseInt(request.getParameter("adddays"));
            thisApptCal.add(Calendar.DATE,addDays);
        }

        scrollValue = 0;
        if (request.getParameter("scrollup")!=null) {
            scrollValue=-15*Integer.parseInt(request.getParameter("scrollup"));
        }
        if (request.getParameter("scrolldown")!=null) {
            scrollValue=15*Integer.parseInt(request.getParameter("scrolldown"));
        }
        if (scrollValue != 0) {
            thisApptCal.add(Calendar.MINUTE,scrollValue);
        }
        
        if (request.getParameter("patientId")!=null && request.getParameter("apptTime")==null) {
            patientId=Integer.parseInt(request.getParameter("patientId"));
        }

        if (request.getParameter("apptId")!=null) {
            if (apptId == Integer.parseInt(request.getParameter("apptId")) && request.getParameter("apptTime")==null) {
                apptId = 0;
            } else {
                apptId=Integer.parseInt(request.getParameter("apptId"));
                if (request.getParameter("apptTime")==null &&  (request.getHeader("referer")!=null && request.getHeader("referer").indexOf(self)<0)) {
                    thisAppointment.setResultSet(io.opnRS("select * from appointments where id =" + apptId));
                    if (thisAppointment.next()) { 
                        String time = thisAppointment.getString("time");
                        int hour = Integer.parseInt(time.substring(0, 2));
                        if (hour>12) {
                            thisApptCal.setStartAM_PM(1);
                        } else {
                            thisApptCal.setStartAM_PM(0);
                        }
                        thisApptCal.setStartHour(hour);
                        thisApptCal.setStartMinute(00);
                        patientId = thisAppointment.getInt("patientId");
                        thisAppointment.beforeFirst();
                    }
                }
            }
            thisApptCal.setSelectedAppointment(apptId);
        }

//        if (request.getParameter("srchString")!=null) {
//            thisApptCal.setSelectedAppointment(0);
//            srchString=request.getParameter("srchString");
//            apptId=0;
//            patientId=0;
//        }
        
        thisAppointment.setResultSet(io.opnRS("select * from appointments where id =" + apptId));
        if (thisAppointment.next()) { 
            patientId = thisAppointment.getInt("patientId");
            thisAppointment.beforeFirst();
        }
        thisPatient.setId(patientId);

    }
    
 //---------------------------------------------------------------------------------------------//
    /**
     * Update the appointment with the values currnetly set
     */
//---------------------------------------------------------------------------------------------//
   public void updateAppointment() throws Exception { 
        thisAppointment.setId(apptId);
        thisAppointment.setPatientId(patientId);
        thisAppointment.setDate(java.sql.Date.valueOf(apptDate));
        thisAppointment.setTime (java.sql.Time.valueOf(apptTime));
        if (apptId==0) { 
            thisAppointment.setType(0);
            intervals=1;
        }
        if (intervals > 0) {
            thisAppointment.setIntervals(intervals);
        }
        if (type > 0) {
            thisAppointment.setType(type);
        }
        thisAppointment.update();
        if (apptId==0) {
            apptId = thisAppointment.getInt("id");
        }
    }    

//---------------------------------------------------------------------------------------------//
    /**
     * Delete the current Appointment.
     */
//---------------------------------------------------------------------------------------------//
    public void deleteAppointment() throws Exception {
        thisAppointment.setResultSet(io.opnUpdatableRS("select * from appointments where id =" + apptId));
        thisAppointment.delete();
        apptId=0;
        thisApptCal.setSelectedAppointment(apptId);
    }
    
//---------------------------------------------------------------------------------------------//
    /**
     * Get the Left Pane of the Page.
     */
//---------------------------------------------------------------------------------------------//
    public String getLeftPane() throws Exception {
        StringBuffer lP = new StringBuffer();

        lP.append(outerHtmTb.startTable());
        lP.append(outerHtmTb.startRow());
        lP.append(outerHtmTb.addCell(getDatePicker()));
        lP.append(outerHtmTb.endRow());
//        lP.append(outerHtmTb.startRow());
//        lP.append(outerHtmTb.addCell(getSearchBubble()));
//        lP.append(outerHtmTb.endRow());
//        lP.append(outerHtmTb.startRow());
//        lP.append(outerHtmTb.addCell(getPatientInfo()));
//        lP.append(outerHtmTb.endRow());
        
        lP.append(outerHtmTb.startRow());
        lP.append(outerHtmTb.addCell(frm.checkBox(thisApptCal.getShowArrivals(),"onClick=\"window.location.href='?showarrivals=' + this.checked \"","showarrivals") + " Show arrivals"));
        lP.append(outerHtmTb.endRow());

        lP.append(outerHtmTb.startRow());
        lP.append(outerHtmTb.addCell(getApptInfo(io, apptId)));
        lP.append(outerHtmTb.endRow());
        lP.append(outerHtmTb.endTable());
        
        return lP.toString();
    }

//---------------------------------------------------------------------------------------------//
    /**
     * Get the rigjht pane of the page
     */
//---------------------------------------------------------------------------------------------//
    public String getRightPane() throws Exception {
        StringBuffer rP = new StringBuffer();

        rP.append(getApptCalendar());

        return rP.toString();
    }
    
//---------------------------------------------------------------------------------------------//
    /**
     * Get the date picker.
     */
//---------------------------------------------------------------------------------------------//
    public String getDatePicker() throws Exception {
        cal.setSelectedDate(thisApptCal.getIsoDate());
        return htmTb.getFrame(htmTb.BOTH,"",platterColor,3,cal.getHtmlCalendar(""));
    }
    
//---------------------------------------------------------------------------------------------//
    /**
     * Get the patient.
     */
//---------------------------------------------------------------------------------------------//
    public Patient getPatient() throws Exception {

        return thisPatient;
    }
    
//---------------------------------------------------------------------------------------------//
    /**
     * Get the search bubble.
     */
//---------------------------------------------------------------------------------------------//
    public String getSearchBubble() throws Exception {

       return thisPatient.getSearchBubble(self);

    }
    
//---------------------------------------------------------------------------------------------//
    /**
     * Get the patient information.
     */
//---------------------------------------------------------------------------------------------//
    public String getPatientInfo() throws Exception {
        String returnValue = thisPatient.getSearchResults(srchString, self, "patientId");
        if (thisPatient.getId() > 0) {
            patientId = thisPatient.getId();
            thisPatient.beforeFirst();
            if (!thisPatient.next()) { return ""; } 
            thisPatient.beforeFirst();
        } 
        return returnValue;
    }
    
//---------------------------------------------------------------------------------------------//
    /**
     * Get the appointment information.
     */
//---------------------------------------------------------------------------------------------//
    public String getApptInfo(RWConnMgr io, int apptId) throws Exception {

        if (apptId==0) { return ""; }

        thisAppointment.beforeFirst();
        if (!thisAppointment.next()) { return ""; }

        thisAppointment.beforeFirst();

        return htmTb.getFrame(htmTb.BOTH,"",platterColor,3,thisAppointment.getMiniInputForm());

    }

//---------------------------------------------------------------------------------------------//
    /**
     * Get the HTML that represents the Appointment Calendar
     */
//---------------------------------------------------------------------------------------------//
    public String getApptCalendar() throws Exception {

        date = "'" + thisApptCal.getIsoDate() +"'";
        String appointmentQuery = "select a.date, time, concat(substr(firstname,1,1), ' ', lastname), " + 
                "a.id, a.type, intervals, ifnull(v.id, 0) as visitid from appointments a join patients b on a.patientid=b.id " + 
                "join appointmenttypes c on a.type =c.id " +
                "left join visits v on a.id=v.appointmentid " +
                "where a.date between " + 
                "date_sub(" + date + ", interval 1 day) and date_add(" + date + ", interval 1 day) " +
                " order by date, time, sequence,id";
        ResultSet lRs = io.opnRS(appointmentQuery);

        thisApptCal.setApptRs(lRs);
 
        return htmTb.getFrame(htmTb.BOTH,"",platterColor,3,htmTb.getTableDiv(435,650,"",thisApptCal.getHtmlGrid()));
    }

//---------------------------------------------------------------------------------------------//
    /**
     * Get the HTML that represents the Appointment Page.
     */
//---------------------------------------------------------------------------------------------//
    public String getHtml(HttpServletRequest newRequest) throws Exception {
        request=newRequest;
        thisAppointment.setResultSet(io.opnRS("select * from appointments where id =" + apptId));
        if (thisAppointment.next()) { 
            patientId = thisAppointment.getInt("patientId");
            thisAppointment.beforeFirst();
        }
        if (patientId!=0) {
            thisPatient.setResultSet(io.opnRS("select * from patients where id =" + patientId));
        }
        StringBuffer html = new StringBuffer();
        String query = "select * from calmsgs where date ='" + thisApptCal.getIsoDate() + "'";
        ResultSet lRs = io.opnRS(query);
        html.append(outerHtmTb.startTable());

        if (lRs.next()) {
            html.append(outerHtmTb.startRow());
            html.append(outerHtmTb.addCell(lRs.getString("message"), "style=\"text-align=center; color=black; background=yellow; font-weight: bold;\" colspan=2"));
            html.append(outerHtmTb.addCell("&nbsp;", "colspan=2"));
            html.append(outerHtmTb.endRow());
        }

        html.append(outerHtmTb.startRow());

        html.append(outerHtmTb.addCell(getLeftPane()));
        html.append(outerHtmTb.addCell(getRightPane()));

        html.append(outerHtmTb.endRow());

        html.append(outerHtmTb.endTable());
        
        return html.toString();
    }

//---------------------------------------------------------------------------------------------//
    /**
     * Set the connection manager.
     */
//---------------------------------------------------------------------------------------------//
    public void setConnMgr(RWConnMgr newIo) throws Exception {
        io=newIo;
    }

//---------------------------------------------------------------------------------------------//
    /**
     * Set the patient.
     */
//---------------------------------------------------------------------------------------------//
    public void setPatient(Patient newPatient) throws Exception {
        if (newPatient.getId()!=thisPatient.getId()) {
            thisApptCal.setSelectedAppointment(0);
        }
        thisPatient=newPatient;
        patientId=thisPatient.getId();
    }

    public void setPlatterColor(String newColor) {
        platterColor = newColor;
    }
}
