/*
 * Comment.java
 *
 * Created on November 28, 2005, 11:46 AM
 *
 * To change this template, choose Tools | Options and locate the template under
 * the Source Creation and Management node. Right-click the template and choose
 * Open. You can then make changes to the template in the Source Editor.
 */

package medical;
import tools.*;
import tools.utils.*;
import java.sql.*;

/**
 *
 * @author BR Online Solutions
 */
public class Visit extends RWResultSet {
    private String id;
    private int appointmentId=0;
    private int patientId=0; 
    private int locationId=0;
    private Date date;
    private RWHtmlTable htmTb   = new RWHtmlTable("100%","0");
    private Timestamp timeStamp;
    private java.util.Date date1;
    private Appointment appointment;
    
    /** Creates a new instance of Visit */
    public Visit() {
        htmTb.replaceNLChar=false;
    }

    public Visit(RWConnMgr io, String ID) throws Exception {
        setConnMgr(io);
        setAppointment();
        setId(ID);
        htmTb.replaceNLChar=false;
    }

    public Visit(RWConnMgr io, int intId) throws Exception {
        setConnMgr(io);
        setAppointment();
        setId(intId);
        htmTb.replaceNLChar=false;
    }

    public void setId(int newId) throws Exception {
        setId("" + newId);
    }
    
    public void setId(String newId) throws Exception {
        id = newId;
        setResultSet(io.opnRS("select * from visits where id=" + id));
        refresh();
    }

    public void setAppointment() throws Exception {
        if(appointment == null) {
            appointment = new Appointment(io, appointmentId);
        } else {
            appointment.setId(appointmentId);
        }
    }
    
    public String getInputForm(String patientId, String visitId) throws Exception {
    // Instantiate an RWInputForm and RWHtmlTable object
        RWInputForm frm = new RWInputForm(rs);
        RWHtmlTable htmTb = new RWHtmlTable ("650", "0");
        StringBuffer vf = new StringBuffer();

    // Set display attributes for the input form
        frm.setTableBorder("0");
        frm.setDftTextBoxSize("20");
        frm.setDftTextAreaCols("80");
        frm.setDftTextAreaRows("10");
        frm.setShowDatePicker(true);
        frm.setDisplayDeleteButton(true);
        frm.setLabelBold(true);
        frm.setUpdateButtonText("  save  ");
        frm.setDeleteButtonText("remove");

    // If adding a comment, Put the familyId and memberId on the form as hidden fields
        if(id.equals("0")) {
            String [] var       = { "patientid", "visitid" };
            String [] val       = { patientId, visitId };
            frm.setPreLoadFields(var);
            frm.setPreLoadValues(val);
        }

    // Get an input item with the record ID to set the rcd and ID fields
        frm.getInputItem("id");
        vf.append(frm.startForm());
        vf.append(htmTb.startTable());
        vf.append(frm.getInputItem("date"));
        vf.append(frm.getInputItem("comment"));
        vf.append(htmTb.endTable());
        vf.append(frm.updateButton());
        vf.append(frm.deleteButton());
        vf.append(frm.showHiddenFields());
        vf.append(frm.endForm());
        
        return vf.toString();
    }

    public int getId() {
        return Integer.parseInt(id);
    }

    public void setLastInsertedRow() throws Exception {
        ResultSet tmpRs  = io.opnRS("select LAST_INSERT_ID()");
        if(tmpRs.next()) {
            setId(tmpRs.getInt(1));
        }
        tmpRs.close();
    }
    
    public String getCharges() throws Exception {
        StringBuffer charges = new StringBuffer();
        charges.append(htmTb.startTable());
        charges.append(htmTb.startRow());
        charges.append(htmTb.addCell("Charges", "class=pageHeading"));
        charges.append(htmTb.endRow());
        charges.append(htmTb.endTable());
        
    // Create an RWFiltered List object to show the occupations
        RWFilteredList lst = new RWFilteredList(io);

    // Create an array with the column headings
        String [] columnHeadings = { "id",  "Procedure"};
        String myQuery = "select a.id, b.description, a.chargeamount from charges a join items b on a.itemid=b.id where visitid = " + id;

    // Set special attributes on the filtered list object
        lst.setTableBorder("0");
        lst.setCellPadding("1");
        lst.setCellSpacing("0");
        lst.setTableWidth("100%");
        lst.setAlternatingRowColors("white","lightgrey");
        lst.setUrlField(0);
        lst.setNumberOfColumnsForUrl(3);
        lst.setRowUrl("charges_d.jsp");
        lst.setOnClickAction("window.open");
        lst.setOnClickOption("\"" + "Charges" + "\",\"width=500,height=200,scrollbars=no,left=100,top=100,\"");
        lst.setOnClickStyle("style=\"cursor: pointer; color: #2c57a7; font-weight: bold;\"");
        lst.setShowRowUrl(true);
        lst.setShowComboBoxes(false);
        lst.setShowColumnHeadings(false);

    // Set specific column widths
        String [] cellWidths = {"0", "100", "100"};
        lst.setColumnWidth(cellWidths);

    // Show the list of charges
        charges.append("<div style=\"width: 400; height: 119; overflow: auto;\">" + lst.getHtml(myQuery, columnHeadings) + "</div>");
        
        return htmTb.getFrame(htmTb.BOTH, "","silver",0,charges.toString());
    }
    public String getProcedures() throws Exception {
        StringBuffer charges = new StringBuffer();
        charges.append(htmTb.startTable());
        charges.append(htmTb.startRow());
        charges.append(htmTb.addCell("Procedures", "class=pageHeading"));
        charges.append(htmTb.endRow());
        charges.append(htmTb.endTable());
        
    // Create an RWFiltered List object to show the occupations
        RWFilteredList lst = new RWFilteredList(io);

    // Create an array with the column headings
        String [] columnHeadings = { "id",  "Item", "Charge"};
        String myQuery = "select a.id, b.description from charges a join items b on a.itemid=b.id where visitid = " + id;

    // Set special attributes on the filtered list object
        lst.setTableBorder("0");
        lst.setCellPadding("1");
        lst.setCellSpacing("0");
        lst.setTableWidth("100%");
        lst.setAlternatingRowColors("white","lightgrey");
        lst.setUrlField(0);
        lst.setNumberOfColumnsForUrl(2);
        lst.setRowUrl("charges_d.jsp");
        lst.setOnClickAction("window.open");
        lst.setOnClickOption("\"" + "Charges" + "\",\"width=500,height=200,scrollbars=no,left=100,top=100,\"");
        lst.setOnClickStyle("style=\"cursor: pointer; color: #2c57a7; font-weight: bold;\"");
        lst.setShowRowUrl(true);
        lst.setShowComboBoxes(false);
        lst.setShowColumnHeadings(false);

    // Set specific column widths
        String [] cellWidths = {"0", "200"};
        lst.setColumnWidth(cellWidths);

    // Show the list of charges
        charges.append("<div style=\"width: 400; height: 119; overflow: auto;\">" + lst.getHtml(myQuery, columnHeadings) + "</div>");
        
        return htmTb.getFrame(htmTb.BOTH, "","silver",0,charges.toString());
    }
    public String getComments() throws Exception {
        StringBuffer comments = new StringBuffer();
        comments.append(htmTb.startTable());
        comments.append(htmTb.startRow());
        comments.append(htmTb.addCell("Notes", "class=pageHeading"));
        comments.append(htmTb.endRow());
        comments.append(htmTb.endTable());
        
    // Create an RWFiltered List object to show the occupations
        RWFilteredList lst = new RWFilteredList(io);

    // Create an array with the column headings
        String [] columnHeadings = { "id",  "Comment"};
        String myQuery = "select id, comment from comments where visitid = " + id;

    // Set special attributes on the filtered list object
        lst.setTableBorder("0");
        lst.setCellPadding("1");
        lst.setCellSpacing("0");
        lst.setTableWidth("100%");
        lst.setAlternatingRowColors("white","lightgrey");
        lst.setUrlField(0);
        lst.setNumberOfColumnsForUrl(3);
        lst.setRowUrl("comments_d.jsp");
        lst.setShowRowUrl(true);
        lst.setOnClickAction("window.open");
        lst.setOnClickOption("\"" + "Comments" + "\",\"width=500,height=200,scrollbars=no,left=100,top=100,\"");
        lst.setOnClickStyle("style=\"cursor: pointer; color: #2c57a7; font-weight: bold;\"");
        lst.setShowComboBoxes(false);
        lst.setShowColumnHeadings(false);

    // Set specific column widths
        String [] cellWidths = {"0", "200"};
        lst.setColumnWidth(cellWidths);

    // Show the list of charges
        comments.append("<div style=\"width: 400; height: 90; overflow: auto;\">" + lst.getHtml(myQuery, columnHeadings) + "</div>");
        
        return htmTb.getFrame(htmTb.BOTH, "","silver",0,comments.toString());
    }

    public String getSOAPNotes() throws Exception {
        StringBuffer notes = new StringBuffer();
        notes.append(htmTb.startTable());
        notes.append(htmTb.startRow());
        notes.append(htmTb.addCell("SOAP Notes", "class=pageHeading"));
        notes.append(htmTb.endRow());
        notes.append(htmTb.endTable());
        
    // Create an RWFiltered List object to show the occupations
        RWFilteredList lst = new RWFilteredList(io);

    // Create an array with the column headings
        String [] columnHeadings = { "id",  "Note"};
        String myQuery = "select id, note from doctornotes where visitid = " + id;

    // Set special attributes on the filtered list object
        lst.setTableBorder("0");
        lst.setCellPadding("1");
        lst.setCellSpacing("0");
        lst.setTableWidth("100%");
        lst.setAlternatingRowColors("white","lightgrey");
        lst.setUrlField(0);
        lst.setNumberOfColumnsForUrl(3);
        lst.setRowUrl("doctornotes_d.jsp");
        lst.setShowRowUrl(true);
        lst.setOnClickAction("window.open");
        lst.setOnClickOption("\"" + "Comments" + "\",\"width=800,height=400,scrollbars=no,left=100,top=100,\"");
        lst.setOnClickStyle("style=\"cursor: pointer; color: #2c57a7; font-weight: bold;\"");
        lst.setShowComboBoxes(false);
        lst.setShowColumnHeadings(false);

    // Set specific column widths
        String [] cellWidths = {"0", "200"};
        lst.setColumnWidth(cellWidths);

    // Show the list of charges
        notes.append("<div style=\"width: 400; height: 90; overflow: auto;\">" + lst.getHtml(myQuery, columnHeadings) + "</div>");
        
        return htmTb.getFrame(htmTb.BOTH, "","silver",0,notes.toString());
    }

    public void setPatientId(int newPatientId) throws Exception {
        patientId = newPatientId;
    }
    
    public void setAppointmentId(int newAppointmentId) throws Exception {
        appointmentId = newAppointmentId;
    }
    
    public void setLocationId(int newLocationId) throws Exception {
        locationId = newLocationId;
    }
    
    public void setDate(Date newDate) throws Exception {
        date = newDate;
    }

    public void setTimestamp() throws Exception {
        String currentDateString = new java.util.Date().toString(); 

        long time1 = date1.parse(currentDateString);
        timeStamp = new java.sql.Timestamp(time1);  
    }

    public void setResultSet(ResultSet newRs) throws Exception {
        super.setResultSet(newRs);
    }

    public void refresh() throws Exception {
        rs.beforeFirst();
        if (rs.next()) {
            id = rs.getString("id");
            appointmentId = rs.getInt("appointmentId");
            patientId = rs.getInt("patientId");
            date = rs.getDate("date");
            locationId = rs.getInt("locationId");
            rs.beforeFirst();
        }
    }

    public void delete() throws Exception {
        rs.beforeFirst();
        if (rs.next()) {
            rs.deleteRow();
        }
    }

    public void update() throws Exception {
        setResultSet(io.opnUpdatableRS("select * from visits where id=" + id));
        rs.beforeFirst();
        setTimestamp();
        if (rs.next()) {
            rs.updateInt("appointmentid", appointmentId);
            rs.updateInt("patientid", patientId);
            rs.updateInt("locationid", locationId);
            rs.updateDate("date", date);
            rs.updateTimestamp("timein", timeStamp);
            rs.updateRow();
         } else {
            setDate(java.sql.Date.valueOf(Format.formatDate(new java.util.Date(), "yyyy-MM-dd")));
            moveToInsertRow();
            rs.updateInt("appointmentid", appointmentId);
            rs.updateInt("patientid", patientId);
            rs.updateInt("locationid", locationId);
            rs.updateDate("date", date);
            rs.updateTimestamp("timein", timeStamp);
            rs.insertRow();
            setLastInsertedRow();
        }
    }

}
