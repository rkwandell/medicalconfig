/*
 * Charge.java
 *
 * Created on November 28, 2005, 11:46 AM
 *
 * To change this template, choose Tools | Options and locate the template under
 * the Source Creation and Management node. Right-click the template and choose
 * Open. You can then make changes to the template in the Source Editor.
 */

package medical;
import java.math.BigDecimal;
import tools.*;
import java.sql.*;
import java.text.SimpleDateFormat;
import java.util.Calendar;

/**
 *
 * @author BR Online Solutions
 */
public class Charge extends RWResultSet {
    private String id;
    private int resourceId = 0;
    private int visitId = 0;
    private int itemId = 0;
    private BigDecimal chargeAmount;
    private String comment;
    private SimpleDateFormat isoFormat = new SimpleDateFormat("yyyy-MM-dd");

    /** Creates a new instance of Charge */
    public Charge() {
    }

    public Charge(RWConnMgr io, String ID) throws Exception {
        setConnMgr(io);
        id = ID;
        setResultSet(io.opnRS("select * from charges where id=" + id));
    }

    public void setId(int newId) throws Exception {
        id = ""+newId;
        setResultSet(io.opnRS("select * from charges where id=" + id));
    }
    
    public void setId(String newId) throws Exception {
        setId(Integer.parseInt(newId));
    }
 
    public void setResourceId(int newResourceId) throws Exception {
        resourceId = newResourceId;
    }

    public void setVisitId(int newVisitId) throws Exception {
        visitId = newVisitId;
    }

    public void setItemId(int newItemId) throws Exception {
        itemId = newItemId;
    }

    public void setChargeAmount(BigDecimal newChargeAmount) throws Exception {
        chargeAmount = newChargeAmount;
    }

    public void update() throws Exception {
        setResultSet(io.opnUpdatableRS("select * from charges where id=" + id));
        rs.beforeFirst();
        if (rs.next()) {
            rs.updateInt("visitid", visitId);
            rs.updateInt("itemid", itemId);
            rs.updateInt("resourceId", resourceId);
            rs.updateBigDecimal("chargeamount", chargeAmount);
            rs.updateRow();
        } else {
            rs.moveToInsertRow();
            rs.updateInt("visitid", visitId);
            rs.updateInt("itemid", itemId);
            rs.updateInt("resourceId", resourceId);
            rs.updateBigDecimal("chargeamount", chargeAmount);
            rs.insertRow();
        }
    }

}
