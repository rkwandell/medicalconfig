/*
 * VisitActivity.java
 *
 * Created on Feb 4, 2006, 12:03 PM
 *
 * To change this template, choose Tools | Options and locate the template under
 * the Source Creation and Management node. Right-click the template and choose
 * Open. You can then make changes to the template in the Source Editor.
 */

package medical;
import tools.*;
import java.sql.*;
import java.util.Calendar;
import java.util.Enumeration;
import javax.servlet.http.HttpServletRequest;


/**
 *
 * @author adepoti
 */
public class VisitActivity extends RWResultSet
{
    private int visitId = 0;
    private int patientId = 0;
    private int noteId = 0;
    private int itemId = 0;
    private int _visitId = 0;
    private String _self = "";
    private RWConnMgr _newIo = null;
    private RWHtmlTable _htmTb = null;
    private Visit _visit = null;
    private Patient _patient = null;
    private String visitFontSize = "";


    //-----------------------------------------------------------------------------------
    /** Default Constructor */
    //-----------------------------------------------------------------------------------
    public VisitActivity()
    {
    }

    
    //-----------------------------------------------------------------------------------
    /** Overloaded Constructor */
    //-----------------------------------------------------------------------------------
    public VisitActivity(RWConnMgr io, Visit visit, Patient patient, int visitId, String self) throws Exception 
    {
        _newIo = io;
        _visit = visit;
        _patient = patient;
        if (visitId==0 && visit!=null) {
            visitId=visit.getId();
        }
        _visitId = visitId;        
        _self = self;
    }
    

    //-----------------------------------------------------------------------------------
    /** gets the HTML for the page */
    //-----------------------------------------------------------------------------------
    public String getHtml() throws Exception
    {
        _patient.setId(_visit.getInt("patientId"));
        
        RWHtmlTable htmTb = new RWHtmlTable("800", "0");
        RWHtmlTable fHtmTb = new RWHtmlTable("800", "0");

        htmTb.setCellSpacing("3");
        StringBuffer thisPage = new StringBuffer();

        htmTb.replaceNewLineChar(false);
        fHtmTb.replaceNewLineChar(false);

        //table for heading
        RWHtmlTable tHtmlTb = new RWHtmlTable("100%", "0");
        thisPage.append(tHtmlTb.startTable());
        tHtmlTb.setWidth("100%");
        thisPage.append(tHtmlTb.startRow() );

        //display the visit date and type as a header
        thisPage.append(htmTb.addCell(
                tHtmlTb.getFrame(htmTb.BOTH, "", "silver", 1, getVisitInfo()),
                "align=center"));
        tHtmlTb.setWidth("233");

        thisPage.append(htmTb.endRow());
        thisPage.append(tHtmlTb.endTable());

        thisPage.append(htmTb.startTable());
        fHtmTb.setWidth("100%");
        thisPage.append(htmTb.startRow());
        fHtmTb.setWidth("233");
        thisPage.append(htmTb.addCell(fHtmTb.getFrame(htmTb.BOTH, "", "silver", 1, _patient.getMiniContactInfo(htmTb, visitFontSize)),"width=233 height=100%")); 
        fHtmTb.setWidth("100%");
        thisPage.append(htmTb.addCell(fHtmTb.getFrame(htmTb.BOTH, "", "silver", 1, getIndicators()) + "<BR>"   + fHtmTb.getFrame(htmTb.BOTH, "", "silver", 1, _visit.getProcedures()) + "<BR>" + fHtmTb.getFrame(htmTb.BOTH, "", "silver", 3, _visit.getSOAPNotes()),"rowspan=3 width=400"));
        thisPage.append(htmTb.addCell(fHtmTb.getFrame(htmTb.BOTH, "", "silver", 1, getItems()),"rowspan=3 width=100"));
        thisPage.append(htmTb.addCell(fHtmTb.getFrame(htmTb.BOTH, "", "silver", 1, getNotes()),"rowspan=3 width=100"));
        thisPage.append(htmTb.endRow());

        thisPage.append(htmTb.startRow("height=77"));
        thisPage.append(htmTb.addCell(fHtmTb.getFrame(htmTb.BOTH, "", "silver", 1, _patient.getPatientProblems(visitFontSize)),"width=233"));
        thisPage.append(htmTb.endRow());


        //added for the Doctor's Notes
        thisPage.append(htmTb.startRow("height=77"));
        thisPage.append(htmTb.addCell(fHtmTb.getFrame(htmTb.BOTH, "", "silver", 1, _patient.getXrayFindings(visitFontSize)),"width=233"));
        thisPage.append(htmTb.endRow());

        thisPage.append(htmTb.startRow("height=180"));
        thisPage.append(htmTb.addCell(fHtmTb.getFrame(htmTb.BOTH, "", "silver", 1, getImages()), "colspan=4  "));
        thisPage.append(htmTb.endRow());

        thisPage.append(htmTb.endTable());

        // I'm going to put this whole thing into a 1 row, 1 cell table that is 100% wide and centered
        fHtmTb.setWidth("825");
        String finalPage = "<Table height=\"100%\" width=\"100%\"><tr><td align=center valign=middle>" + 
                           fHtmTb.getFrame(htmTb.BOTH, "", "white", 1, thisPage.toString()) + 
                           "</td></tr></table>";
        return finalPage;
    }

    
    //-----------------------------------------------------------------------------------
    /** inserts a charge */
    //-----------------------------------------------------------------------------------
    public void insertCharge(HttpServletRequest request, RWConnMgr io, int itemId, Visit visit, Patient patient) throws Exception 
    {
        StringBuffer noteText = new StringBuffer();
        ResultSet lRs = io.opnRS("select * from items where id = " + itemId);
        lRs.next();
        if (!lRs.getString("comment").equals("")) {
            Comment newComment = new Comment(io, "0");
            newComment.setDate(Calendar.getInstance());
            newComment.setPatientId(patient.getId());
            newComment.setVisitId(visit.getId());
            newComment.setComment(lRs.getString("comment"));
            newComment.update();
            insertNote(lRs.getString("comment"));
        }
        Charge thisCharge = new Charge(io, "0");
        thisCharge.setVisitId(visit.getId());
        thisCharge.setItemId(itemId);
        thisCharge.setResourceId(0);
        thisCharge.setChargeAmount(lRs.getBigDecimal("amount"));
        thisCharge.update();

     // Roll through the checked items and append to the doctor note.
        Enumeration parms = request.getParameterNames();
        String name;
        boolean oneWasAdded=false;
        while (parms.hasMoreElements()) {
            name=(String)parms.nextElement();
            if (name.substring(0,3).equals("si_")) {
                lRs = io.opnRS("select * from subitems where id = " + name.substring(3));
                if (lRs.next()) {
                    if (oneWasAdded) {
                        noteText.append(", ");
                    }
                    noteText.append(lRs.getString("subitem"));
                    oneWasAdded=true;
                }
            }
        }
        if (oneWasAdded) {
            noteText.append(".");
            insertNote(noteText.toString());
        }
        
    }


    //-----------------------------------------------------------------------------------
    /** inserts a note */
    //-----------------------------------------------------------------------------------
    public void insertNote(RWConnMgr io, int noteId, Visit visit, Patient patient) throws Exception 
    {
        ResultSet lRs = io.opnRS("select * from doctornotes where visitid = " + visit.getId());
            String commentId = "0";
        if (lRs.next()) {
            commentId = lRs.getString("id");
        } 

        DoctorNote thisDoctorNote = new DoctorNote(io, commentId);
        String commentText = "";
        if (thisDoctorNote.next()) {
            commentText=thisDoctorNote.getString("note");
        }
        lRs = io.opnRS("select * from notetemplates where id = " + noteId);
        String noteText = "";
        if (lRs.next()) {
            noteText = lRs.getString("notetext");
        } else {
            noteText = "NOTE TEXT NOT DEFINED";
        }
        thisDoctorNote.setNoteDate(Calendar.getInstance());
        thisDoctorNote.setPatientId(patient.getId());
        thisDoctorNote.setVisitId(visit.getId());
        if (commentText.equals("")) {
            thisDoctorNote.setNote(noteText);
        } else {
            thisDoctorNote.setNote(commentText + " " + noteText);
        }
        thisDoctorNote.update();
    }

    

    //-----------------------------------------------------------------------------------
    /** inserts a note for a patient */
    //-----------------------------------------------------------------------------------
    private void insertNote(int noteId) throws Exception 
    {
        ResultSet lRs = io.opnRS("select * from doctornotes where visitid = " + _visit.getId());
        String commentId = "0";
    
        if (lRs.next()) 
        {
            commentId = lRs.getString("id");
        } 

        DoctorNote thisDoctorNote = new DoctorNote(_newIo, commentId);
        String commentText = "";
        if (thisDoctorNote.next()) 
        {
            commentText = thisDoctorNote.getString("note");
        }

        lRs = _newIo.opnRS("select * from notetemplates where id = " + noteId);
        String noteText = "";

        if (lRs.next()) 
        {
            noteText = lRs.getString("notetext");
        } 
        else 
        {
            noteText = "NOTE TEXT NOT DEFINED";
        }
        thisDoctorNote.setNoteDate(Calendar.getInstance());
        thisDoctorNote.setPatientId(_patient.getId());
        thisDoctorNote.setVisitId(_visit.getId());
        if (commentText.equals("")) {
            thisDoctorNote.setNote(noteText);
        } 
        else 
        {
            thisDoctorNote.setNote(commentText + " " + noteText);
        }
        thisDoctorNote.update();
    }

    
    //-----------------------------------------------------------------------------------
    /** inserts a note for a patient */
    //-----------------------------------------------------------------------------------
    private void insertNote(String noteText) throws Exception 
    {
        ResultSet lRs = _newIo.opnRS("select * from doctornotes where visitid = " + _visit.getId());
        String commentId = "0";
        String commentText = "";
    
        if (lRs.next()) {
            commentId = lRs.getString("id");
        } 

        DoctorNote thisDoctorNote = new DoctorNote(_newIo, commentId);
        if (thisDoctorNote.next()) {
            commentText = thisDoctorNote.getString("note");
        }

        thisDoctorNote.setNoteDate(Calendar.getInstance());
        thisDoctorNote.setPatientId(_patient.getId());
        thisDoctorNote.setVisitId(_visit.getId());
        if (commentText.equals("")) {
            thisDoctorNote.setNote(noteText);
        } else {
            thisDoctorNote.setNote(commentText + " " + noteText);
        }
        thisDoctorNote.update();
    }

    
    //-----------------------------------------------------------------------------------
    /** insert a charge */
    //-----------------------------------------------------------------------------------
    private void insertCharge(int itemId) throws Exception 
    {
        ResultSet lRs = _newIo.opnRS("select * from items where id = " + itemId);
        lRs.next();
        if (!lRs.getString("comment").equals("")) {
            Comment newComment = new Comment(io, "0");
            newComment.setDate(Calendar.getInstance());
            newComment.setPatientId(_visit.getInt("patientid"));
            newComment.setVisitId(_visit.getId());
            newComment.setComment(lRs.getString("comment"));
            newComment.update();
            insertNote(lRs.getString("comment"));
        }
        Charge thisCharge = new Charge(_newIo, "0");
        thisCharge.setVisitId(_visit.getId());
        thisCharge.setItemId(itemId);
        thisCharge.setResourceId(0);
        thisCharge.setChargeAmount(lRs.getBigDecimal("amount"));
        thisCharge.update();
    }


    //-----------------------------------------------------------------------------------
    /** constructs the row for the images for a patient */
    //-----------------------------------------------------------------------------------
    private String getImages() throws Exception 
    {
        StringBuffer xr   = new StringBuffer();
        int id            = _patient.getId();
        RWHtmlTable htmTb = new RWHtmlTable("800", "0");
        htmTb.replaceNewLineChar(false);

        _patient.refresh();

        htmTb.setWidth("100%");

        xr.append("<div style=\" height: 300; width=850; overflow: auto;\">");
        xr.append(htmTb.startTable());
        xr.append(htmTb.startRow("height=280"));

        ResultSet lRs = _newIo.opnRS("Select * from patientdocuments where patientId=" + id + " and documentType=1 and identifierid=1 order by seq");
        while (lRs.next()) {
            xr.append(htmTb.addCell(getImage(htmTb, lRs.getString("documentpath"), lRs.getString("description"), lRs.getInt("id")), htmTb.CENTER, "width=200"));
        }
        lRs.close();

//        xr.append(htmTb.addCell(getImage(htmTb, id, 1, 1), htmTb.CENTER, "width=200"));
//        xr.append(htmTb.addCell(getImage(htmTb, id, 1, 2), htmTb.CENTER, "width=200"));
//        xr.append(htmTb.addCell(getImage(htmTb, id, 1, 3), htmTb.CENTER, "width=200"));
//        xr.append(htmTb.addCell(getImage(htmTb, id, 1, 4), htmTb.CENTER, "width=200"));

        xr.append(htmTb.endRow());
        xr.append(htmTb.endTable());
        xr.append("</div>");


        return xr.toString();
    }

    
    //-----------------------------------------------------------------------------------
    /** gets specific image for a patient */
    //-----------------------------------------------------------------------------------
    public String getImage(RWHtmlTable htmTb, String documentPath, String description, int id) throws Exception {
        String imagePath = "images/no_photo.jpg";
        String imageDescription = "No Photo";
        String onClickOption = "";
        StringBuffer img = new StringBuffer();


        if(!documentPath.equals("")) {
            imagePath        = documentPath.replaceAll("\\\\", "/");
            imagePath        = "/medicaldocs" + imagePath.substring(imagePath.lastIndexOf("/medical/") + "/medical".length());
            imageDescription = description;
            onClickOption    = "onClick=displayImage('" + id + "') style='cursor: pointer;'";
        }


        htmTb.setWidth("100%");
        img.append(htmTb.startTable());
        img.append(htmTb.startRow());
        img.append(htmTb.addCell("<image src=\"" + imagePath + "\" height=280 " + onClickOption + ">", htmTb.CENTER));
        img.append(htmTb.endRow());
        img.append(htmTb.endTable());

        return img.toString();
    }   

    private String getImage(RWHtmlTable htmTb, int patientId, int documentType, int identifierId) throws Exception 
    {
        String imagePath = "images/no_photo.jpg";
        String imageDescription = "No Photo";
        String onClickOption = "";
        StringBuffer img = new StringBuffer();

        ResultSet lRs = _newIo.opnRS("Select * from patientdocuments where patientId=" + _patient.getId() + " and documentType=" + documentType + " and identifierid=" + identifierId);

        if(lRs.next()) {
            imagePath        = lRs.getString("documentpath");
            imageDescription = lRs.getString("description");
            onClickOption    = "onClick=displayImage('" + lRs.getString("id") + "') style='cursor: pointer;'";
        }

        lRs.close();

        img.append(htmTb.startTable());
        img.append(htmTb.startRow());
        img.append(htmTb.addCell("<image src=" + imagePath + " height=280 " + onClickOption + ">", htmTb.CENTER));
        img.append(htmTb.endRow());
        img.append(htmTb.endTable());

        return img.toString();
    }
    
    
    //-----------------------------------------------------------------------------------
    /** gets items */
    //-----------------------------------------------------------------------------------
    private String getItems() throws Exception 
    {

        StringBuffer items   = new StringBuffer();
        String onClick       = "";
        RWHtmlTable htmTb = new RWHtmlTable("100%", "0");
        htmTb.replaceNewLineChar(false);
        ResultSet lRs = _newIo.opnRS("select id, buttontext, buttoncolor, buttontextcolor, subitemtype, comment from items where showitem order by sequence desc");
        RWHtmlForm frm = new RWHtmlForm();

        htmTb.setWidth("70");

        items.append(htmTb.startTable());

        items.append(htmTb.startRow());
        items.append(htmTb.addCell("Procedures", "class=pageHeading"));
        items.append(htmTb.endRow());
        items.append(htmTb.endTable());

        items.append("<div style=\"width: 100; height: 290; overflow: auto;\">");
        items.append(htmTb.startTable());
        while (lRs.next()) {
            if (lRs.getInt("subitemtype")>0) {
                onClick = "\"window.open('showsubitems.jsp?&itemId=" + lRs.getString("id") + "&subitemtypeid=" + lRs.getString("subitemtype") + "&pageHeader=" + lRs.getString("comment") + "','SubItems','width=600,height=500,scrollbars=no,left=50,top=20,')\"";
            } else {
                onClick = "\"window.location.href='" + _self + "?itemId=" + lRs.getString("id") + "'\"";
            }
            items.append(htmTb.startRow());
            items.append(htmTb.addCell(frm.button(lRs.getString("buttontext"), "style=\"font-size: 10px;    background: " 
                     + lRs.getString("buttoncolor") + "    ; color:" + lRs.getString("buttontextcolor") 
                     + "; width=80px; font-weight: bold; \" onClick=" + onClick)));
            items.append(htmTb.endRow());
        }
        items.append("</div>"); 
        items.append(htmTb.endTable());

        return items.toString();
    }

    
    //-----------------------------------------------------------------------------------
    /** gets Notes */
    //-----------------------------------------------------------------------------------
    private String getNotes() throws Exception 
    {
        StringBuffer items   = new StringBuffer();
        RWHtmlTable htmTb = new RWHtmlTable("100%", "0");
        htmTb.replaceNewLineChar(false);
        ResultSet lRs = _newIo.opnRS("select id, buttontext, buttoncolor, buttontextcolor from notetemplates where showitem order by sequence desc");
        RWHtmlForm frm = new RWHtmlForm();

        htmTb.setWidth("70");

        items.append(htmTb.startTable());

        items.append(htmTb.startRow());
        items.append(htmTb.addCell("SOAP", "align=center class=pageHeading"));
        items.append(htmTb.endRow());
        items.append(htmTb.endTable());

        items.append("<div style=\"width: 100; height: 290  ; overflow: auto;\">");
        items.append(htmTb.startTable());
        while (lRs.next()) {
            items.append(htmTb.startRow());
            items.append(htmTb.addCell(frm.button(lRs.getString("buttontext"), "style=\"font-size: 10px; background: " 
                     + lRs.getString("buttoncolor") + "    ; color:" + lRs.getString("buttontextcolor") 
                     + "; width=80px; font-weight: bold; \" onClick=\"window.location.href='" 
                     + _self + "?noteId=" + lRs.getString("id") + "'\"")));
            items.append(htmTb.endRow());
        }
        items.append("</div>"); 
        items.append(htmTb.endTable());

        return items.toString();
    }

    
    //-----------------------------------------------------------------------------------
    /** gets the visit info */
    //-----------------------------------------------------------------------------------
    private String getVisitInfo() throws Exception
    {
        final String WALK_IN = "Walk In";
        final String spacer = "  on  ";
        String apptType = "";
        String apptDate = "";

        ResultSet lRs = _newIo.opnRS("SELECT * FROM visitsummary  WHERE id=" + _visitId);

        //walk in if there is appointment

        if(lRs.next()) 
        {
            apptType        = lRs.getString("type");
            apptDate        = lRs.getString("date");
        }

        lRs.close();

        if ((apptType == null) || (apptType.length() <= 0))
        {
            apptType = WALK_IN;
        }

        return "<a style=\"font-size: 14px\">" + apptType + spacer + apptDate + "</a>";
    }
    
    
    //-----------------------------------------------------------------------------------
    /** gets the patient indicators in read only mode */
    //-----------------------------------------------------------------------------------
    private String getIndicators() throws Exception
    {
        PatientIndicators pi = new PatientIndicators( _newIo, _patient.getId() );
        return pi.getViewOnlyPatientIndicators();
    }

    //-----------------------------------------------------------------------------------
    /** gets the DoctorNotes */
    //-----------------------------------------------------------------------------------
    private String getDoctorNotes() throws Exception
    {
        StringBuffer sy   = new StringBuffer();
        RWHtmlTable htmTb = new RWHtmlTable();
        RWInputForm frm   = new RWInputForm();

        ResultSet rs = _newIo.opnRS("select * from doctornotes where visitid=" + _visitId);

        sy.delete(0, sy.length());
        sy.append(htmTb.startTable("100%", "0"));
        String onClickLocationA = "onClick=window.open(\"doctornotes_d.jsp?id=";
        String onClickLocationB = "\",\"DoctorNotes\",\"width=500,height=225,left=150,top=200,toolbar=0,status=0\"); ";
        String linkClass = " style=\"cursor: pointer; color: #030089;\"";

        sy.append(htmTb.roundedTop(1,"","#030089","doctornotedivision"));

        // Display the heading
        sy.append(htmTb.startRow());
        sy.append(htmTb.headingCell("DoctorNotes", "style=\"cursor: pointer\" " + onClickLocationA + "0&patientid=" + _patient.getId() + onClickLocationB));
        sy.append(htmTb.endRow());
        
        //  End the table for the comments heading
        sy.append(htmTb.endTable());

        // Start a division for the doctor notes section
        sy.append("<div style=\"width: 100%; height: 75;  overflow: auto; text-align: left;\">\n");

        // List the doctor notes
        sy.append(htmTb.startTable("94%", "0"));

        while(rs.next()) 
        {
            String link = onClickLocationA + rs.getString("id") + onClickLocationB;
            sy.append(htmTb.startRow());
            sy.append(htmTb.addCell(rs.getString("note"), htmTb.LEFT, link + linkClass, ""));
            sy.append(htmTb.endRow());
        }
        sy.append(htmTb.endTable());
        
        // End the division
        sy.append("</div>\n");

        return sy.toString();
    }

    //-----------------------------------------------------------------------------------
    /** Sets the visit font size */
    //-----------------------------------------------------------------------------------
    public void setVisitFontSize(String newFontSize) throws Exception
    {
        visitFontSize = newFontSize;
    }

}
