/*
 * PatientPlan.java
 *
 * Created on November 28, 2005, 1:56 PM
 *
 * To change this template, choose Tools | Options and locate the template under
 * the Source Creation and Management node. Right-click the template and choose
 * Open. You can then make changes to the template in the Source Editor.
 */

package medical;
import tools.*;
import tools.utils.*;
import java.sql.*;

/**
 *
 * @author rwandell
 */
public class PatientPlan extends RWResultSet {
    private RWHtmlTable htmTb           = new RWHtmlTable("400", "0");
    private int planId                  = 0;
    private int patientId               = 0;
    private StringBuffer pln            = new StringBuffer();
    private StringBuffer ins            = new StringBuffer();
    private StringBuffer inpFrm         = new StringBuffer();
    private RWHtmlTable tbl             = new RWHtmlTable("275", "0");
    private RWInputForm frm             = new RWInputForm();
    
    /** Creates a new instance of PatientPlan */
    public PatientPlan() {
    }
    
    public PatientPlan(RWConnMgr io, String newId) throws Exception {
        setConnMgr(io);
        setPlanId(newId);
    }
    
    public PatientPlan(RWConnMgr io, int newId) throws Exception {
        setConnMgr(io);
        setPlanId(newId);
    }
    
    public void setPlanId(int newId) throws Exception {
        planId = newId;
        setResultSet(io.opnUpdatableRS("select * from patientplan where id=" + planId));
        if(planId != 0) { refresh(); }
        if(planId == 0) { clearValues(); }
    }

    public void setPlanId(String newId) throws Exception {
        setPlanId(Integer.parseInt(newId));
    }
    
    public void setPatientId(int newPatient) {
        patientId = newPatient;
    }
    
    public void refresh() throws Exception {
        beforeFirst();
        next();
        setPatientId(getInt("patientId"));
    }
    
    public void clearValues() {
        patientId = 0;
    }
    
    public String getInputForm(String patientId) throws Exception {
        inpFrm.delete(0, inpFrm.length());
        pln.delete(0, pln.length() );
        ins.delete(0, ins.length());
        
        beforeFirst();
       
        // Instantiate an RWInputForm and RWHtmlTable object
        RWInputForm frm = new RWInputForm(rs);

    // Set display attributes for the input form
        frm.setDftTextBoxSize("20");
        frm.setDftTextAreaCols("35");
        frm.setDisplayDeleteButton(true);
        frm.setShowDatePicker(true);
        frm.setLabelBold(true);
        frm.setUpdateButtonText("  save  ");
        frm.setDeleteButtonText("remove");

    // Set display attributes for the table
        tbl.replaceNewLineChar(false);

    // If adding a comment, Put the familyId and memberId on the form as hidden fields
        if(planId == 0) {
            String [] var       = { "patientid" };
            String [] val       = { patientId };
            frm.setPreLoadFields(var);
            frm.setPreLoadValues(val);
        }
        
        frm.getInputItem("id");
        
        inpFrm.append(frm.startForm());

    // Show the dates and number of visits associated with the plan
        String [] planInfo = getPlanInfo();
        pln.append(tbl.startTable());
        pln.append(frm.getInputItem("planid"));
        pln.append(frm.getInputItem("startdate"));
        pln.append(frm.getInputItem("enddate"));
        pln.append(frm.getInputItem("frequency"));
        pln.append(frm.getInputItem("visits", "onBlur=totalVisits()"));
        pln.append(frm.getInputItem("previousvisits", "onBlur=totalVisits()"));
        pln.append(htmTb.startRow());
        pln.append(htmTb.addCell("<b>Visits to date</b"));
        pln.append(htmTb.addCell(frm.textBox(planInfo[5], "visitsToDate", "10", "10", "class=tBoxText READONLY")));
        pln.append(htmTb.endRow());
        pln.append(htmTb.startRow());
        pln.append(htmTb.addCell("<b>Visits Remaining</b"));
        pln.append(htmTb.addCell(frm.textBox(planInfo[4], "visitsRemaining", "10", "10", "class=tBoxText READONLY")));
        pln.append(htmTb.endRow());
        pln.append(tbl.endTable());
        
    // Show the insurance information
        String totalAmount = "0.00";
        String remainingCovered = "0";
        if(planId !=0) {
            totalAmount = Format.formatNumber(getFloat("patientportion") + getFloat("insuranceportion"), "#####0.00; (#####0.00)");
            remainingCovered = Format.formatNumber(getInt("visitsallowed") - Integer.parseInt(planInfo[5]), "#####0; (#####0)");
        }
        ins.append(tbl.startTable());
        ins.append(frm.getInputItem("patientportion", "onBlur=totalCharges()"));
        ins.append(frm.getInputItem("insuranceportion", "onBlur=totalCharges()"));
        ins.append(htmTb.startRow());
        ins.append(htmTb.addCell("<b>Total Amount</b>"));
        ins.append(htmTb.addCell(frm.textBox(totalAmount, "totalAmount", "12", "12", "class=tBoxText READONLY")));
        ins.append(htmTb.endRow());
        ins.append(frm.getInputItem("visitsallowed", "onBlur=totalVisits()"));
        ins.append(htmTb.startRow());
        ins.append(htmTb.addCell("<b>Remaining Visits Covered</b>"));
        ins.append(htmTb.addCell(frm.textBox(remainingCovered, "remainingCovered", "10", "10", "class=tBoxText READONLY")));
        ins.append(htmTb.endRow());
        ins.append(frm.getInputItem("planoptions"));
        ins.append(tbl.endTable());
        
    // Add the plan and insurance information to the input form
        inpFrm.append(htmTb.startTable());
        inpFrm.append(htmTb.startRow());
        inpFrm.append(htmTb.startCell(htmTb.LEFT, "class=infoHeader"));
        inpFrm.append("Treatment Plan Details");
        inpFrm.append(tbl.getFrame("#cccccc", pln.toString()));
        inpFrm.append(htmTb.endCell());
        inpFrm.append(htmTb.startRow());
        inpFrm.append(htmTb.startCell(htmTb.LEFT, "class=infoHeader"));        
        inpFrm.append("Insurance Information");
        inpFrm.append(tbl.getFrame("#cccccc", ins.toString()));
        inpFrm.append(htmTb.endCell());
        inpFrm.append(htmTb.endRow());
        inpFrm.append(htmTb.endTable());
        
    // Add the buttons to the bottom of the form
        inpFrm.append(frm.updateButton());
        inpFrm.append(frm.deleteButton());
        inpFrm.append(frm.showHiddenFields());
        inpFrm.append(frm.endForm());
        
        return inpFrm.toString();
    }

    public String getMiniPlanDetails(int newPatient) throws Exception {
        setPatientId(newPatient);
        StringBuffer rp = new StringBuffer();
        String onClickLocation = "onClick=window.open(\"plan_d.jsp?patientid=" + patientId + "&planid=" + planId +
                              "\",\"Plan\",\"width=345,height=380,left=150,top=200,toolbar=0,status=0,\"); " +
                              " colspan=3 style=\"cursor: pointer\"";
        
        rp.append(htmTb.startTable("100%"));
        rp.append(htmTb.startRow());
        rp.append(htmTb.headingCell("Treatment Plan", onClickLocation));
        rp.append(htmTb.endRow());
        rp.append(htmTb.endTable());

    // Start a division for the symptoms section
        rp.append("<div style=\"width: 100%; height: 83;  overflow: auto; text-align: left;\">\n");
        beforeFirst();
        if(next()) {
        
            rp.append(htmTb.startTable("100%"));

            if(patientId !=0 && planId != 0) {
                String [] planInfo = getPlanInfo();
                rp.append(htmTb.startRow());
                rp.append(htmTb.addCell("<b>Last Visit</b>", "style=\"color: white\""));
                rp.append(htmTb.addCell(planInfo[0], "style=\"color: white\""));
                rp.append(htmTb.addCell("Remaining: " + planInfo[4], "style=\"color: white\""));
                rp.append(htmTb.endRow());
                rp.append(htmTb.startRow());
                rp.append(htmTb.addCell("<b>Frequency:</b>", "style=\"color: white\""));
                rp.append(htmTb.addCell(frm.textBox(planInfo[1], "frequency", "25", "25","class=tBoxText READONLY"), "colspan=2"));
                rp.append(htmTb.endRow());
                rp.append(htmTb.startRow());
                rp.append(htmTb.addCell("<b>Start:</b>", "style=\"color: white\""));
                rp.append(htmTb.addCell(frm.textBox(Format.formatDate(planInfo[2], "MM/dd/yyyy"), "planstartdate", "10", "10","class=tBoxText READONLY"), "colspan=2"));
                rp.append(htmTb.endRow());
                rp.append(htmTb.startRow());
                rp.append(htmTb.addCell("<b>End:</b>", "style=\"color: white\""));
                rp.append(htmTb.addCell(frm.textBox(Format.formatDate(planInfo[3], "MM/dd/yyyy"), "planenddate", "10", "10","class=tBoxText READONLY"), "colspan=2"));
                rp.append(htmTb.endRow());
            }
            rp.append(htmTb.endTable());
        }
        rp.append("</div>");
        
        return rp.toString();
    }
    
    public void updatePlanDetails() throws Exception {
        boolean rowUpdated = false;
        if(rs.next()) {
            ResultSet pRs = io.opnRS("select * from plantypes where id=" + rs.getString("planid"));
            if(pRs.next()) {
                if(rs.getString("frequency").equals("")) { 
                    rs.updateString("frequency", pRs.getString("frequency"));
                    rowUpdated = true;
                }
                if(rs.getInt("visits") == 0) {
                    rs.updateInt("visits", pRs.getInt("visits"));
                    rowUpdated = true;
                }
            }
            pRs.close();
            if(rowUpdated) { rs.updateRow(); }
        }
    }
    
    public String [] getPlanInfo() throws Exception {
        String [] planInfo = { "N/A", "N/A", "N/A", "N/A", "N/A", "0" };

        int numVisits      = 0;
        if(planId !=0) {
            planInfo[1] = rs.getString("frequency");
            planInfo[2] = rs.getString("startdate");
            planInfo[3] = rs.getString("enddate");
            planInfo[4] = rs.getString("visits");
            
            ResultSet vRs = io.opnRS("select max(date) from visits where patientid=" + getInt("patientId"));
            if(vRs.next()) {
                planInfo[0] = Format.formatDate(vRs.getString(1), "MM/dd/yyyy");
            }
            
            vRs = io.opnRS("select * from patientplanvisits where patientid=" + getInt("patientId"));
            if(vRs.next()) {
                planInfo[5] = vRs.getString("visits");
                numVisits = getInt("visits") - vRs.getInt("visits") - getInt("previousvisits");
                planInfo[4] = "" + numVisits;
            }
            
            vRs.close();
        }
        return planInfo;
    }
    
}
