/*
 * Payment.java
 *
 * Created on February 10, 2006, 10:58 PM
 *
 * To change this template, choose Tools | Options and locate the template under
 * the Source Creation and Management node. Right-click the template and choose
 * Open. You can then make changes to the template in the Source Editor.
 */

package medical;
import java.math.BigDecimal;
import java.util.Date;
import tools.*;


/**
 *
 * @author BR Online Solutions
 */
public class Payment extends RWResultSet {
    RWConnMgr io;

    private int id                  = 0;
    private int provider;
    private int checkNumber;
    
    private BigDecimal amount;

    private int chargeId;
    private int patientId;

    private Date date;
    
    /** Creates a new instance of Payment */
    public Payment() {
    }

    public Payment(RWConnMgr io, int newId) throws Exception {
        setConnMgr(io);
        setId(id);
    }   
    
    public void setConnMgr(RWConnMgr newIo) throws Exception {
        if(io == null) {
            io = newIo;
        }
    }
    
    public void setId(int newId) throws Exception {
        id = newId;
        setResultSet(io.opnRS("select * from payments where id=" + id));
        refresh();
    }
    
    public void setId(String newId) throws Exception {
        setId(Integer.parseInt(newId));
    }
    
    public void refresh() throws Exception {
        if(next()) {
            setProvider(getInt("provider"));
            setCheckNumber(getInt("checknumber"));
            setAmount(getBigDecimal("amount"));
        }
        beforeFirst();
    }

    public int getId() throws Exception {
        return id;
    }
    
    public int getProvider() throws Exception {
        return provider;
    }
    
    public int getCheckNumber() throws Exception {
        return checkNumber;
    }
    
    public BigDecimal getAmount() throws Exception {
        return amount;
    }
    
    public int getChargeId() throws Exception {
        return chargeId;
    }

    public int getPatientId() throws Exception {
        return patientId;
    }

    public Date getDate() throws Exception {
        return date;
    }

    public void setProvider(int newProvider) {
        provider = newProvider;
    }
    
    public void setCheckNumber(int newCheckNumber) {
        checkNumber = newCheckNumber;
    }
    
    public void setAmount(BigDecimal newAmount) {
        amount = newAmount;
    }
    
    public void setChargeId(int newChargeId) {
        chargeId = newChargeId;
    }
    
    public void setPatientId(int newPatientId) {
        patientId = newPatientId;
    }
    
    public void setDate(Date newDate) {
        date = newDate;
    }

    public void update() throws Exception {
        setResultSet(io.opnUpdatableRS("select * from payments where id=" + id));
        
        if(next()) {
            updateInt("provider", provider);
            updateInt("checknumber", checkNumber);
            updateBigDecimal("amount", amount);
            updateInt("chargeid", chargeId);
            updateInt("patientid", patientId);
            updateDate("date", (java.sql.Date)date);
            updateRow();
        } else {
            moveToInsertRow();
            updateInt("provider", provider);
            updateInt("checknumber", checkNumber);
            updateBigDecimal("amount", amount);
            updateInt("chargeid", chargeId);
            updateInt("patientid", patientId);
            updateDate("date", (java.sql.Date)date);
            insertRow();
        }
    }
    
}
