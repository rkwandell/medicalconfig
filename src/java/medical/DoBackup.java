/*
 * DoBackup.java
 *
 * Created on October 10, 2007, 4:46 PM
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */
 
package medical;
import java.io.File;
import java.sql.ResultSet;
import tools.RWDocument;
 
/**
 *
 * @author blaine
 */
public class DoBackup {
    private static String sourcePath="";
    private static String targetPath="";
    private static RWDocument documentCopier=new RWDocument();
 
    // Empty constructor
    public DoBackup(){    
    }
    
    public static void main(String [] args) throws Exception {
    // data
        sourcePath="C:\\Program Files\\MySQL\\MySQL Server 5.0\\data";
        targetPath="C:\\Backups\\" + tools.utils.Format.formatDate(new java.util.Date(), "yyyy-MM-dd") + "\\data";
        copyDocs(new File(getSourcePath() + "\\medical"));
        sourcePath="C:\\Program Files\\MySQL\\MySQL Server 5.0\\data";
        targetPath="C:\\Backups\\" + tools.utils.Format.formatDate(new java.util.Date(), "yyyy-MM-dd") + "\\data";
        copyDocs(new File(getSourcePath() + "\\rwcatalog"));
 
    // code
        sourcePath="C:\\Program Files\\Apache Software Foundation\\Tomcat 5.5\\webapps";
        targetPath="C:\\Backups\\" + tools.utils.Format.formatDate(new java.util.Date(), "yyyy-MM-dd") + "\\code";
        copyDocs(new File(getSourcePath() + "\\medical"));
        sourcePath="C:\\Program Files\\Apache Software Foundation\\Tomcat 5.5\\webapps";
        targetPath="C:\\Backups\\" + tools.utils.Format.formatDate(new java.util.Date(), "yyyy-MM-dd") + "\\code";
        copyDocs(new File(getSourcePath() + "\\medicalconfig"));
    }
    
    public void backup(String source, String target) throws Exception {
        if(source == null || target == null || source.trim().equals("") || target.trim().equals("")) {
            setSourcePath(source);
            setTargetPath(target);
            backup();
        }
    }
    
    public void backup() throws Exception {
        copyDocs(new File(getSourcePath()));
    }
 
    public static void copyDocs(File inDirectory) throws Exception {
        String fileName = "";
        String targetDir = "";
        String sourceFile = "";
        String targetFile = "";
        File tmpDir;
        
        File[] docs = inDirectory.listFiles();
 
        if (docs == null) {
            System.out.println("Directory does not exist or is not a Directory");
        } else {
            targetFile=inDirectory.getCanonicalPath();
            targetFile=targetFile.substring(sourcePath.length());
            targetFile=getTargetPath() + targetFile;
            System.out.println(targetFile);
            tmpDir = new File(targetFile);
            if (!tmpDir.exists()) {
                tmpDir.mkdirs();
                for (int i=0; i<docs.length; i++) {
                    if (docs[i].isDirectory()) {
                        copyDocs(docs[i]);
                    } else {
                        fileName = docs[i].getName();
                        sourceFile = docs[i].getCanonicalPath();
                        targetFile=docs[i].getCanonicalPath();
                        targetFile=targetFile.substring(sourcePath.length());
                        targetFile=getTargetPath() + targetFile;
                        System.out.println(targetFile);
                        documentCopier.copyDocument(sourceFile, targetFile);
                    }
                }
            } else {
                System.out.println("Directory " + targetFile + " already exists.  Cannot perform Backup");
            }
        }
    }    

    public static String getSourcePath() {
        return sourcePath;
    }

    public static void setSourcePath(String aSourcePath) {
        sourcePath = aSourcePath;
    }

    public static String getTargetPath() {
        return targetPath;
    }

    public static void setTargetPath(String aTargetPath) {
        targetPath = aTargetPath;
    }
}