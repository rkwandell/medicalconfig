/*
 * Symptoms.java
 *
 * Created on December 27, 2005, 1:27 PM
 *
 * To change this template, choose Tools | Options and locate the template under
 * the Source Creation and Management node. Right-click the template and choose
 * Open. You can then make changes to the template in the Source Editor.
 */

package medical;
import tools.*;
import tools.utils.*;
import java.sql.*;
import java.util.ArrayList;

/**
 *
 * @author BR Online Solutions
 */
public class PatientInsurance extends RWResultSet {
    private StringBuffer ins        = new StringBuffer();
    private RWHtmlTable htmTb       = new RWHtmlTable();
    private RWInputForm frm         = new RWInputForm();
    private int id                  = 0;
    private int patientId           = 0;
    private int providerId          = 0;
    private int relationship        = 0;
    private String providerNumber   = "";
    private String providerGroup    = "";
    
    /** Creates a new instance of Symptoms */
    public PatientInsurance() {
    }
    
    public PatientInsurance(RWConnMgr newIo) throws Exception {
        setConnMgr(newIo);        
    }
    
    public void setId(int newId) throws Exception {
        id = newId;
        refresh();
    }
    
    public void setPatientId(int newId) {
        patientId = newId;
    }
    
    public void refresh() throws Exception {
        rs = io.opnUpdatableRS("select * from patientinsurance where id=" + id);
        if(next()) {
            patientId = getInt("patientid");
            providerId = getInt("providerid");
            providerNumber = getString("providernumber");
            providerGroup = getString("providerGroup");
            relationship = getInt("relationshipid");
        }
        
        beforeFirst();
    }
    
    public String getInputForm() throws Exception {
        beforeFirst();
        ins.delete(0, ins.length());
        frm.setResultSet(this);
    
    // Set Arrays for hidden fields
        String [] var = { "patientid" };
        String [] val = { "" + patientId };
        frm.setPreLoadFields(var);
        frm.setPreLoadValues(val);
        
    // Set display attributes for the input form
        frm.setDftTextBoxSize("20");
        frm.setDftTextAreaCols("35");
        frm.setDisplayDeleteButton(true);
        frm.setShowDatePicker(true);
        frm.setLabelBold(true);
        frm.setUpdateButtonText("  save  ");
        frm.setDeleteButtonText("remove");

        ins.append(frm.startForm());
        frm.getInputItem("id");
        
        ins.append(htmTb.startTable("100%", "0"));
        
        ins.append(frm.getInputItem("providerid"));
        ins.append(frm.getInputItem("providernumber"));
        ins.append(frm.getInputItem("providergroup"));
        ins.append(frm.getInputItem("relationshipid"));
        
        ins.append(htmTb.endTable());
        
        ins.append(frm.updateButton());
        ins.append(frm.deleteButton());
        ins.append(frm.showHiddenFields());
        
        ins.append(frm.endForm());
        
        return ins.toString();
    }
    
    public String getInputForm(String newId) throws Exception {
        setId(Integer.parseInt(newId));
        return getInputForm();
    }
    
    public String getInputForm(int newId) throws Exception {
        setId(newId);
        return getInputForm();
    }
    
    public String getPatientInsuranceList(int patientId) throws Exception {
        rs = io.opnRS("select * from insurancedisplay where patientid=" + patientId);
        ins.delete(0, ins.length());
        ins.append(htmTb.startTable("94%", "0"));
        String onClickLocationA = "onClick=window.open(\"patientinsurance_d.jsp?id=";
        String onClickLocationB = "\",\"Insurance\",\"width=500,height=125,left=150,top=200,toolbar=0,status=0,\"); ";
        String linkClass = " style=\"cursor: pointer; color: #030089;\"";

        ins.append(htmTb.roundedTop(4,"","#030089","insurancedivision"));

        // Display the heading
        ins.append(htmTb.startRow("style=\"cursor: pointer\" " + onClickLocationA + "0&patientId=" + patientId + onClickLocationB));
        ins.append(htmTb.headingCell("Provider", htmTb.LEFT, "width=40%"));
        ins.append(htmTb.headingCell("Insured's Id", htmTb.LEFT, "width=20%"));
        ins.append(htmTb.headingCell("Group Number", htmTb.LEFT, "width=20%"));
        ins.append(htmTb.headingCell("Relationship", htmTb.LEFT, "20%"));
        ins.append(htmTb.endRow());
    //  End the table for the Insurance heading
        ins.append(htmTb.endTable());

    // Start a division for the details section
        ins.append("<div style=\"width: 100%; height: 44;  overflow: auto; text-align: left;\">\n");

    // List the symptoms
        ins.append(htmTb.startTable("94%", "0"));

        while(next()) {
            String link = onClickLocationA + getString("id") + onClickLocationB;
            ins.append(htmTb.startRow());
            ins.append(htmTb.addCell(getString("name"), htmTb.LEFT, link + linkClass + " width=40%", ""));
            ins.append(htmTb.addCell(getString("providernumber"), htmTb.LEFT, "width=20%", ""));
            ins.append(htmTb.addCell(getString("providergroup"), htmTb.LEFT, "width=20%", ""));
            ins.append(htmTb.addCell(getString("relationship"), htmTb.LEFT, "width=20%"));
            ins.append(htmTb.endRow());
        }
        ins.append(htmTb.endTable());
    // End the division
        ins.append("</div>\n");
        
        return ins.toString();
    }
}
