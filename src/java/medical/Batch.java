/*
 * Batch.java
 *
 * Created on February 28, 2006, 08:41 PM
 *
 * To change this template, choose Tools | Options and locate the template under
 * the Source Creation and Management node. Right-click the template and choose
 * Open. You can then make changes to the template in the Source Editor.
 */

package medical;
import java.math.BigDecimal;
import java.util.Date;
import tools.*;


/**
 *
 * @author BR Online Solutions
 */
public class Batch extends RWResultSet {
    RWConnMgr io;

    private int id                  = 0;
    private int provider            = 0;

    private String description;

    private Date created;
    private Date billed;
    
    /** Creates a new instance of Payment */
    public Batch() {
    }

    public Batch(RWConnMgr io, int newId) throws Exception {
        setConnMgr(io);
        setId(newId);
    }   
    
    public void setConnMgr(RWConnMgr newIo) throws Exception {
        if(io == null) {
            io = newIo;
        }
    }
    
    public void setId(int newId) throws Exception {
        id = newId;
        setResultSet(io.opnRS("select * from batches where id=" + id));
        refresh();
    }
    
    public void setId(String newId) throws Exception {
        setId(Integer.parseInt(newId));
    }
    
    public void refresh() throws Exception {
        if(next()) {
            setDescription(getString("description"));
            setCreated(getDate("created"));
            setBilled(getDate("billed"));
            setProvider(getInt("provider"));
        }
        beforeFirst();
    }

    public int getId() throws Exception {
        return id;
    }

    public int getProvider() throws Exception {
        return provider;
    }
    
    public String getDescription() throws Exception {
        return description;
    }
    
    public Date getCreated() throws Exception {
        return created;
    }
    
    public Date getBilled() throws Exception {
        return billed;
    }
    
    public void setDescription(String newDescription) {
        description = newDescription;
    }
    
    public void setCreated(Date newDate) {
        created = newDate;
    }

    public void setBilled(Date newDate) {
        billed = newDate;
    }

    public void setProvider(int newProvider) {
        provider = newProvider;
    }

    public void update() throws Exception {
        setResultSet(io.opnUpdatableRS("select * from batches where id=" + id));
        java.util.Date today = new java.util.Date();

        if(next()) {
            updateString("description", description);
            updateInt("provider", provider);
            if (created!=null) {
                updateDate("created", new java.sql.Date(created.getTime()));
            } else {
                updateDate("created", null);
            }
            if (billed!=null) {
                updateDate("billed", new java.sql.Date(billed.getTime()));
            } else {
                updateDate("billed", null);
            }
            updateRow();
        } else {
            moveToInsertRow();
            updateString("description", description);
            updateInt("provider", provider);
            updateDate("created", new java.sql.Date(today.getTime()));
            if (billed!=null) {
                updateDate("billed", new java.sql.Date(billed.getTime()));
            } else {
                updateDate("billed", null);
            }
            insertRow();
            io.setMySqlLastInsertId();
            setId(io.getLastInsertedRecord());
        }
    }
    public void delete() throws Exception {
        setResultSet(io.opnUpdatableRS("select * from batches where id=" + id));
        beforeFirst();
        if (next()) {
            deleteRow();
        }
        setId(0);
    }    
}
