/*
 * Comment.java
 *
 * Created on November 28, 2005, 11:46 AM
 *
 * To change this template, choose Tools | Options and locate the template under
 * the Source Creation and Management node. Right-click the template and choose
 * Open. You can then make changes to the template in the Source Editor.
 */

package medical;
import tools.*;
import tools.utils.*;
import java.sql.*;
import java.text.SimpleDateFormat;
import java.util.Calendar;

/**
 *
 * @author rwandell
 */
public class Comment extends RWResultSet {
    private String id;
    private int patientId = 0;
    private int visitId = 0;
    private Date date;
    private String comment;
    private SimpleDateFormat isoFormat = new SimpleDateFormat("yyyy-MM-dd");

    /** Creates a new instance of Comment */
    public Comment() {
    }

    public Comment(RWConnMgr io, String ID) throws Exception {
        setConnMgr(io);
        id = ID;
        setResultSet(io.opnRS("select * from comments where id=" + id));
    }

    public String getInputForm(String patientId, String visitId) throws Exception {
    // Instantiate an RWInputForm and RWHtmlTable object
        RWInputForm frm = new RWInputForm(rs);
        RWHtmlTable htmTb = new RWHtmlTable ("650", "0");
        StringBuffer cf = new StringBuffer();

    // Set display attributes for the input form
        frm.setTableBorder("0");
        frm.setDftTextBoxSize("20");
        frm.setDftTextAreaCols("80");
        frm.setDftTextAreaRows("10");
        frm.setShowDatePicker(true);
        frm.setDisplayDeleteButton(true);
        frm.setLabelBold(true);
        frm.setUpdateButtonText("  save  ");
        frm.setDeleteButtonText("remove");

    // If adding a comment, Put the familyId and memberId on the form as hidden fields
        if(id.equals("0")) {
            String [] var       = { "patientid", "visitid" };
            String [] val       = { patientId, visitId };
            frm.setPreLoadFields(var);
            frm.setPreLoadValues(val);
        }

    // Get an input item with the record ID to set the rcd and ID fields
        frm.getInputItem("id");
        cf.append(frm.startForm());
        cf.append(htmTb.startTable());
        cf.append(frm.getInputItem("date"));
        cf.append(frm.getInputItem("comment"));
        cf.append(htmTb.endTable());
        cf.append(frm.updateButton());
        cf.append(frm.deleteButton());
        cf.append(frm.showHiddenFields());
        cf.append(frm.endForm());
        
        return cf.toString();
    }

    public void setPatientId(int newPatientId) throws Exception {
        patientId = newPatientId;
    }

    public void setVisitId(int newVisitId) throws Exception {
        visitId = newVisitId;
    }

    public void setDate(Date newDate) throws Exception {
        date = newDate;
    }

    public void setDate(Calendar newDate) throws Exception {
        date = java.sql.Date.valueOf(isoFormat.format(newDate.getTime()));
    }

    public void setComment(String newComment) throws Exception {
        comment = newComment;
    }

    public void update() throws Exception {
        setResultSet(io.opnUpdatableRS("select * from comments where id=" + id));
        rs.beforeFirst();
        if (rs.next()) {
            rs.updateInt("patientid", patientId);
            rs.updateInt("visitid", visitId);
            rs.updateDate("date", date);
            rs.updateString("comment", comment);
            rs.updateRow();
        } else {
            rs.moveToInsertRow();
            rs.updateInt("patientid", patientId);
            rs.updateInt("visitid", visitId);
            rs.updateDate("date", date);
            rs.updateString("comment", comment);
            rs.insertRow();
        }
    }

}
